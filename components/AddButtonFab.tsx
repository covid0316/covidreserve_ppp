const AddButtonFab = ({ label, className='', style={}, onClick=undefined }: { label: string, className?: string, style?: object, onClick?: (event: any) => void }) => {
  return (
    <div style={style} className={`fabbutton ${className}`} onClick={onClick}>
      <div className='fab'>
        <span style={{ margin: 'auto' }}>
          <svg
            width='14'
            height='14'
            viewBox='0 0 14 14'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'
          >
            <path
              d='M14.3309 8.88H8.89891V14.648H6.08491V8.88H0.680906V6.318H6.08491V0.577999H8.89891V6.318H14.3309V8.88Z'
              fill='white'
            />
          </svg>
        </span>
      </div>
      <span className='fablabel'>{label}</span>
    </div>
  );
};

export default AddButtonFab;
