/* eslint-disable react/jsx-no-bind */
import { Field, ErrorMessage } from 'formik';
import { QuestionProps, CheckboxQuestion } from './QuestionTypes';

const onChangeHandler = (e, question, id, setFieldValue) => {
  if (id === question.other) {
    setFieldValue(id, e.target.checked === false ? false : '');
  } else {
    setFieldValue(id, e.target.checked);
  }
};

const Checkbox: QuestionProps<CheckboxQuestion> = ({
  question,
  setFieldValue,
  values,
  className,
  errors
}) => (
  <div className='questionsection'>
    <span>
      <div className={className}>{question.question}</div>
      {question.tooltip && (
        <div className='formtooltip'>{question.tooltip}</div>
      )}
    </span>
    <div className='fieldsection'>
      {question.fields!.map(field => (
        <div
          className='field'
          key={field.id}
          style={{
            display:
              field.showCondition && field.showCondition(values) === false
                ? 'none'
                : 'block'
          }}
        >
          <Field
            type='checkbox'
            name={field.id}
            onChange={e => {
              if (field.onChangeHandler) {
                field.onChangeHandler(setFieldValue!, e);
              } else {
                onChangeHandler(e, question, field.id, setFieldValue);
              }
            }}
          />
          <span className='field'>{field.displayName}</span>
          <ErrorMessage component="div" className="formik-error" name={field.id} />
        </div>
      ))}
      {question.other ? (
        <>
          <Field
            checked={values[question.other] !== false}
            type='checkbox'
            name={question.other}
            onChange={e =>
              onChangeHandler(e, question, question.other, setFieldValue)
            }
          />
          <span className='field'>{'Other:'}</span>
          <span className='othertextbox'>
            <Field
              disabled={values[question.other] === false}
              type='textbox'
              name={question.other}
              value={values[question.other] || ''}
            />
          </span>
        </>
      ) : (
        <div />
      )}
      {errors && errors[question.id!] ? (
        <div className='formik-error'>{errors[question.id!]}</div>
      ) : null}
    </div>
  </div>
);

export default Checkbox;
