/* eslint-disable react/jsx-no-bind */
import { Field, ErrorMessage } from 'formik';
import * as types from './QuestionTypes';
import { findDOMNode } from 'react-dom';

function onChangeHandler (question, id, setFieldValue, fieldHandler, prefix = '') {
  if (question)
  // Set all fields to false
  question.fields.forEach((field) => {
    setFieldValue(prefix + field.id, false);
  });

  if (question.other) {
    setFieldValue(prefix + question.other, false);
  }

  // Set this field to true
  if (id === question.other) {
    setFieldValue(prefix + id, true);
    setFieldValue(prefix + id + '-string', '');
  } else {
    setFieldValue(prefix + id, true);
    if (question.other) {
      setTimeout(() => {
        setFieldValue(prefix + question.other + '-string', '');
        setFieldValue(prefix + question.other + '-string', undefined)
      }); // hack: async set to "" --> undefined to clear it
    }
  }

  if (typeof fieldHandler === 'function') {
    fieldHandler(setFieldValue);
  }
};

const Radio: types.QuestionProps<types.RadioQuestion> = ({ question, setFieldValue, values, className, num, prefix, errors }) => {
  const prefixStr = typeof num === 'number' && typeof prefix === 'string' ? `${prefix}.${num}.` : '';
  let questionErrors;
  if (errors && prefix && question.id && typeof num === 'number') {
    questionErrors = (errors?.[prefix]?.[num]?.[question.id]);
  } else if (errors && question.id && errors[question.id]) {
    questionErrors = errors[question.id];
  }
  return (
  <div className='questionsection' style={{ display: (question.showCondition && question.showCondition(values) === false) ? 'none' : 'block' }}>
    <span>
      <div className={className}>{question.question}</div>
      {question.tooltip && <div className='formtooltip'>{question.tooltip}</div>}
    </span>
    <div className='fieldsection'>
    {question.fields!.map((field) => (
      <div
        className='field'
        key={field.id}
        style={{ display: field.showCondition && field.showCondition(values) === false ? 'none' : 'block' }}
      >
        <input
          checked={!!(prefixStr ? values[prefix!][num][field.id] : values[field.id])}
          type="radio"
          name={prefixStr + field.id}
          onChange={(e) => {
            onChangeHandler(question, field.id, setFieldValue, field.onChangeHandler, prefixStr);
          }}
        />
        <span className='field' style={{ display: 'inline' }}>{field.displayName}</span>
        <ErrorMessage component="div" className="formik-error" name={prefixStr + field.id} />
      </div>
    ))}
    {question.other ? (
      <div className='field'>
        <Field
          checked={prefixStr ? false !== values[prefix!][num][question.other] : false !== values[question.other]}
          type="radio"
          name={prefixStr + question.other}
          onChange={(e) => onChangeHandler(
            question,
            question.other,
            setFieldValue,
            undefined, prefixStr
          )}
        />
        <span className='field'>{'Other:'}</span>
        <span className='othertextbox'>
          <Field
            flag='other'
            disabled={values[question.other] === false}
            type="textbox"
            name={prefixStr + question.other + '-string'}
          />
          <ErrorMessage component="div" className="formik-error" name={prefixStr + question.other + '-string'}/>
        </span>
      </div>
    ) : (
      <div />
    )}

    {questionErrors && (
        <div className={`formik-error ${className}`}>{questionErrors}</div>
      )}
    </div>
  </div>
)};

export default Radio;
