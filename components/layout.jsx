import React, { useContext } from 'react';
import Head from 'next/head';
import fetchSwal from '../lib/fetchSwal';
import { UserContext } from './UserContext';
import { Nav } from './nav.tsx';
import redirectTo from '../lib/redirectTo';
import { useRouter } from 'next/router';

const siteMap = [
  ['Instructions', '/form/instructions'],
  ['Form 5', '/form/5/0'],
  ['Form 1590', '/form/1590'],
  ['Form 1368', '/form/1368'],
  ['Form 413D', '/form/413D'],
  ['Form 4506-T', '/form/4506-T'],
  ['Form 2202', '/form/2202'],
  ['Summary', '/form/summary'],
];

export default ({ hardcodedCheckBoxes = false, children, style = {} }) => {
  const router = useRouter();
  const {
    state: { isLoggedIn },
    dispatch,
  } = useContext(UserContext);
  const handleLogout = (event) => {
    event.preventDefault();
    fetchSwal
      .delete('/api/session')
      .then((data) => data.ok !== false && dispatch({ type: 'clear' }))
      .then(() => redirectTo('/'));
  };

  return (
    <>
      <Head>
        <title>Covid Reserve</title>
        <meta
          key="viewport"
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900&amp;display=swap" rel="stylesheet"></link>
        <meta property="og:title" content="Covid Reserve" />
      </Head>
            <div className="mobile-ins">

              <p>Please fill in the form on the desktop for a better experience</p>

            </div>

      <header>
        <Nav siteMap={siteMap} isLoggedIn={isLoggedIn} onLogOut={handleLogout} shouldAddCheckboxesDeleteMe={hardcodedCheckBoxes} />
      </header>

      <main style={style}>{children}</main>
      <footer>
        <p>©{new Date().getFullYear()} Covid Reserve. All rights reserved.</p>
      </footer>
      <style jsx>
        {`
          main {
            max-width: 80%;
            margin: 80px auto 0 auto;
            background-color: white;
            padding: 64px;
            border-radius: 20px;
          }
                    @media (max-width: 576px) {

                      main{

                        display: none;

                      }

                    }

          footer {
            text-align: center;
            font-size: 0.8rem;
            margin-top: 1rem;
            padding: 3rem;
            color: #888;
          }

                    @media (min-width: 577px) {

                      .mobile-ins p {

                        display: none;

                      }

                    }

                    @media (max-width: 576px) {

                      .mobile-ins p {

                          position: absolute;

                          transform: translate(-50%, -50%);

                          top: 50%;

                          left: 50%;

                          text-align: center;

                          font-size: 19px;

                      }

                      .mobile-ins {

                          width: 100%;

                          position: absolute;

                          top: 58px;

                          background: #eae9e9;

                          height: 100%;

                      }

                    }


        `}
      </style>
    </>
  );
};
