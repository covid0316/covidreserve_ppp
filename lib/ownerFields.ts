import { addressFieldsNoLease } from "./addressFields";
import { Question, RadioQuestion, Field } from "../components/QuestionTypes";
import * as Yup from 'yup';

/* eslint-disable prefer-template */

export const businessOwnerFields = (prefix: string) => [
  {
    type: 'text',
    formno: [5],
    question: 'Business Entity Owner Name:',
    id: prefix + '-name',
  },
  {
    type: 'text',
    formno: [5],
    question: 'EIN:',
    id: prefix + '-ein',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Type of Business:',
    id: prefix + '-type',
  },
  {
    type: 'text',
    formno: [5],
    question: '% Owned',
    id: prefix + '-percentage',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Email Address:',
    id: prefix + '-email',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Telephone Number (with area code):',
    id: prefix + '-phone',
  },
  ...addressFieldsNoLease(prefix),
] as Question[];

export const ownerFields = (prefix: string) => [
  {
    type: 'text',
    formno: [5],
    question: 'Legal Name:',
    id: prefix + '-legal-name',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Title / Office:',
    id: prefix + '-title',
  },
  {
    type: 'text',
    formno: [5],
    question: '% Owned',
    id: prefix + '-percentage',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Email Address:',
    id: prefix + '-email',
  },
  {
    type: 'radio',
    formno: [5],
    question: 'Is this owner a US Citizen',
    id: prefix,
    fields: [
      {
        displayName: 'Yes',
        id: prefix + '-citizen-yes',
      },
      {
        displayName: 'No',
        id: prefix + '-citizen-no',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    question: 'SSN / EIN:',
    id: prefix + '-ssn',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Marital Status:',
    id: prefix + '-marital-status',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Date of Birth:',
    id: prefix + '-dob',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Place of Birth:',
    id: prefix + '-birth-place',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Telephone Number (with area code):',
    id: prefix + '-phone',
  },
  ...addressFieldsNoLease(prefix),
] as Question[];

function atLeastOneOf(id: string, list: string[]) {
  return Yup.boolean().test(id, "At least one option should be selected", function() {
    const p = this.parent;
    const result = Object.keys(p).some((f => {
      return !!p[f] && list.includes(f)
    }));
    return result;
  });
}

export const ownerSchema = (id: string, fields: Question[]) => {
 const list = fields.filter(field => field.type === 'radio').map((field) => [...(field as unknown as RadioQuestion).fields.map(f => f.id)]).reduce((arr, v) => ([...arr, ...v]), []);
  let schema = Yup.object().shape({
    [id]: atLeastOneOf(id, list),
    ...fields.reduce((obj, field) => {
      if (field.type === 'text') {
        return { ...obj, [field.id]: Yup.string().required('Required')}
      } else if (field.type === 'radio') {
        return field.fields.reduce((obj2, radioField) => {
          return { ...obj2, [radioField.id]: Yup.boolean().required('Required') }
        }, obj);
      }
      return {};
    }, {} as object)
  });

  return Yup.object().shape({
    [id]: Yup.array().of(schema)
  });
}
