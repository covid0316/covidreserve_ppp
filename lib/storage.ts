import fetchSwal from './fetchSwal';

if (typeof window === 'undefined') {
    (global as any).window = {
        sessionStorage: {
            getItem: () => {},
            setItem: () => {}
        }
    };
}

export const flattenListForm = (form: object): object =>
    Object.entries(form).reduce(
        (acc, step) => Object.entries(step[1] as any).reduce((acc2, entry) => {
          acc2[entry[0]] = entry[1];
          return acc2;
        }, acc),
      {});

export const FormStorageHelper = {
  set: async (values: object, form: string | number, step?: number | string, formName?: string) => {
    await fetchSwal.post('/api/form', { form, step, values, formName }, { noToast: true });
  },
  get: async (form: string | number, step?: string | number, formName?: string) => {
    return await fetchSwal.get('/api/form', { form, step, formName }, { noToast: true });
  },
  delete: async (form: string | number, formName: string) => {
      return await fetchSwal.delete('/api/form', { form, formName }, { noToast: true });
  }
};
