import { TaskEither } from 'fp-ts/lib/TaskEither';
import * as E from 'fp-ts/lib/Either';

export function fromPromise<E, T>(promise: Promise<T>): TaskEither<E, T> {
  return () => promise.then(E.right).catch(E.left);
}