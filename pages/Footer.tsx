import React, {useContext, useState} from 'react';
import {UserContext} from '../components/UserContext';
import Layout from '../components/homeLayout';
import clsx from "clsx";
import Link from "next/link";
import {Logo} from '../components/Icons';
import Grid from '@material-ui/core/Grid';
type User = {
    email: string,
    password: string
}


const Footer = () => {
    const {dispatch} = useContext(UserContext);
    const [showAdditional, setShowAdditional] = useState(false);

    const handleAdditionalFaq = () => {
        setShowAdditional(!showAdditional)
    }

    return (
        <div className="col-sm-12">
            <div className="row">
            <div className=" section4" style={{borderBottom: '1px solid #DCDDDE'}}>
            <Grid
              container
              style={{ textAlign: 'center', padding: '0', maxWidth:'1264px', marginLeft: 'auto', marginRight: 'auto'}}
            >
              <Grid item xs={12} sm={7}>
                <div style={{fontSize: '24px', fontWeight:300, paddingBottom: '0px', color: '#1382E9', textAlign: 'left'}}>
                    Ready to start your application?
                </div>
                <div className="callouts get_setup_footer" style={{fontWeight:400}}>
                    Contact us or create an account. 
                </div>
              </Grid>
              <Grid item xs={12} sm={5} style={{display: 'flex', justifyContent: 'left', alignItems: 'center',}}>
                <Link href="/signup">
                    <a className="button" style={{fontSize: '16px', backgroundColor: '#1382E9', color: '#FFF', marginRight: '24px'}}>Start now</a>
                </Link>
                <Link href="tel:888-315-5784">
                    <a className="button" style={{fontSize: '16px', backgroundColor: '#FFF', color: '#1382E9'}}>Contact sales</a>
                </Link>
                </Grid>
            </Grid>
            </div>
            </div>
            <div className="row">
                <div className="col-sm-12 footer">
                <div className=" section5">
                <Grid
                  container
                  style={{padding: '0', maxWidth:'1264px', marginLeft: 'auto', marginRight: 'auto'}}
                >
                  <Grid item xs={12} md={4}>
                    <Link href=""><a>{Logo}</a></Link>
                    <div style={{fontSize: '12px', marginTop: '8px'}}>©2020 CovidReserve</div>
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <div className="footer-header" style={{fontWeight: 700, fontSize: "14px", color: '#6F7274'}}>About</div>
                    <Link href="/faq"><a style={{color: '#6F7274', fontSize: "14px"}}><div>Frequently Asked Questions</div></a></Link>
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <div className="footer-header" style={{fontWeight: 700, fontSize: "14px", color: '#6F7274'}}>Partners</div>
                    <Link href="tel:888-315-5784"><a style={{color: '#6F7274', fontSize: "14px"}}><div>Call Sales: (888) 315-5784</div></a></Link>
                  </Grid>
                </Grid>
                </div>
                </div>
            </div>

            <style jsx>
                {`
                @media only screen and (max-width: 960px) {
                  .get_setup_footer {
                    font-size: 24px !important;
                  }
                  .section4  .get_setup_footer{
                    padding-bottom: 24px !important;
                  }
                  .footer-header{
                    margin-top: 16px !important;
                  }
                }

                button:hover,
                button:active {
                  transform: translate3d(0px, -1px, 0px);
                  box-shadow: 0 8px 30px rgba(0, 0, 0, 0.12);
                }

                h2 {
                  color: #333;
                  text-align: center;
                }

                 button {
                  display: block !important;
                  margin-bottom: 0.5rem !important;
                  color: blue !important;
                  border-radius: 5px !important;
                  border: none !important;
                  cursor: pointer !important;
                  transition: all 0.2s ease 0s !important;
                  padding: 10px 25px !important;
                  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.12) !important;
                }

                button:hover,
                button:active {
                  transform: translate3d(0px, -1px, 0px) !important;
                  box-shadow: 0 8px 30px rgba(0, 0, 0, 0.12) !important;
                }



          .faq :global(a.button),
          .faq :global(a.button:visited),
          .faq :global(a.button:active) {
            display: inline-block;
            color: white;
            background: #1382E9;
            border-radius: 5px;
            padding: 8px 16px;
            font-size: 20px;
          }


          .float-right {
            background-color: white;
            position: absolute;
            right: 50%;
            margin-right: -671px;
            width: 464px;
            height: 644px;
            top: 16px;
            border: 1px solid #4B6075;
            box-sizing: border-box;
            border-radius: 5px;
            box-sizing: border-box;
            padding: 0 20px;
          }
          .float-right h2 {
            font-size: 34px;
            font-weight: normal;
            margin-bottom: 0;
          }
          .float-right h3 {
            font-size: 16px;
            font-weight: normal;
            margin-top: 5px;
            margin-bottom: 10px;
            text-align: center;
            color: #4B6075;
          }
          .inner-column {
            position: relative;
            width: 1200px;
            margin: 0 auto;
          }
          p {
            text-align: center;
            color: #888;
          }
          .hero-tag {
            color: white;
            position: absolute;
            bottom: 52px;
            width: 820px;
            margin-left: -600px;
            left: 50%;
            font-style: normal;
            font-weight: bold;
            font-size: 48px;
            line-height: 142%;
          }
          .hero-img-wrapper {
            position: relative;
            height: 676px;
            overflow: hidden;
            width: 100%;
          }
          .hero-gradient {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            background: none;
            background: linear-gradient(180deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0.2) 51%, rgba(0,0,0,0.45) 100%);
          }
          .hero-img {
            width: 100%;
            min-width: 1824px;
          }
          .section1 {
            background: white;
            padding: 80px 0;
          }
          .callouts {
            font-color: color: #000;
            font-size: 48px;
            text-align: left;
            padding: 0 80px 20px 80px;
          }
          .section1 .table {
            margin-top: 32px;
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
          }
          .section1 .table > div {
            text-align: center;
            width: 300px;
            font-size: 20px;
          }
          .section1 .table .title {
            font-size: 24px;
            font-weight: bold;
            margin-bottom: 8px;
          }
          .section1 .table .icon {
            display: inline-block;
            height: 80px;
            width: 80px;
            margin-bottom: 24px;
          }
          .section2 {
            width: 1200px;
            background: white;
            display: flex;
            flex-direction: row;
            margin-top: 140px;
            margin: 140px auto 160px auto;
          }
          .section2 .left {
            width: 544px;
            font-size: 24px;
            padding: 80px 40px;
          }
          .section2 .quote {
            margin-bottom: 40px;
          }
          .section4 :global(a.button),
          .section4 :global(a.button:visited),
          .section4 :global(a.button:active) {
            display: inline-block;
            border-radius: 5px;
            padding: 8px 16px;
            font-size: 20px;
          }



                     .section4  .get_setup_footer {
                       position: static;
                       left: 0%;
                       right: 0%;
                       top: 0%;
                       bottom: 0%;

                       font-family: Lato;
                       font-style: normal;
                       font-weight: bold;
                       font-size: 32px;
                       /* or 60px */
                       text-align: left;

                       /* White */
                       color: #000;

                       /* Inside Auto Layout */
                       flex: none;
                       order: 0;
                       align-self: center;
                       margin: 0px 0px;
                    }

          .section2 .quoteText {
            margin-bottom: 50px;
          }
          .section3 {
            text-align: center;
            padding-bottom: 140px;
            display: none;
          }
          .section3 .pricing-small {
            color: #2CA01C;
            font-size: 34px;
            margin-top: 20px;
          }
          .section3 .pricing-large {
            display: flex;
            margin-top: 20px;
            align-items: center;
            justify-content: center;
          }
          .section3 .price {
            color: #2CA01C;
            font-size: 120px;
            font-weight: bold;
          }
          .section3 .strike {
            font-size: 96px;
            text-decoration: line-through;
            padding-left: 60px;
          }
          .section3 .unit {
            font-size: 48px;
            padding-left: 18px;
          }
          .section3 .disclaimer {
            font-size: 24px;
            margin-bottom: 25px;
          }
          .section4 {
            background: #F3F5F6;
            padding: 48px 16px;
            color: black;
            text-align: center;
            width:100%;
          }
          .section5 {
            background: #F3F5F6;
            padding: 16px 16px;
            color: black;
            width:100%;
          }
          .section4 .callouts {
            padding: 0;
            padding-bottom: 0px;
          }
          .section4 :global(a.button),
          .section4 :global(a.button:visited),
          .section4 :global(a.button:active) {
            background: white;
            color: #116DC1;
          }
          .footer {
            padding: 32px 0;
            background: #F3F5F6;
          }
          .footer .inner {
            display: flex;
            margin: 0 auto;
          }
          .footer :global(a),
          .footer :global(a:visited),
          .footer :global(a:active) {
            color: white;
          }
          ul {
            list-style-type: none;
          }
          li :global(a) {
            font-size: 16px
          }
          li {
            padding: 12px;
          }
        `}
            </style>


        </div>


    );
};


export default Footer;
