import React from 'react';
import Head from 'next/head';
import App from 'next/app';
import { UserContextProvider } from '../components/UserContext';
import './global.css';


class MyApp extends App {

    constructor(props){
      super(props);
      this.state = {
      };
    }

    componentDidMount() {
        require('jquery');
        require('popper.js');
        require('bootstrap');
    }

  render() {
    var { Component, pageProps } = this.props;
    pageProps.url = this.state.url;
    return (
      <UserContextProvider>
        <Head>
          <title>Covid Reserve</title>
          <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900&amp;display=swap" rel="stylesheet"></link>
        </Head>
        <Component {...pageProps} />
      </UserContextProvider>
    );
  }
}

export default MyApp;
