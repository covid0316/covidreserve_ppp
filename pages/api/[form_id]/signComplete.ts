import { pipe } from 'fp-ts/lib/pipeable';
import { FpConnect, authenticate } from '../../../lib/tsNextConnect';
import { DocspringClient, FpDocspringClient, DataRequestToken } from '../../../lib/docspringClient';
import * as TE from 'fp-ts/lib/TaskEither';
import * as E from 'fp-ts/lib/Either';
import middleware from '../../../middlewares/middleware';
import { fromPromise } from '../../../lib/util';
import { flattenListForm } from '../../../lib/storage';
import { PdfRequest, Form413D } from '../data/pdfTypes';

const handler = new FpConnect();

handler.useMiddleware(middleware);
handler.get<void, void>((req, res) => {
  const db = req.db!;
  const user = req.user;
  const { submission_id, form_id } = req.query;

  if (user && submission_id && form_id) {
    const { _id: userId } = user;
    return pipe(
      fromPromise(db.collection("pdfs").updateOne(
        { _id: userId },
        { $set: { _id: user._id, [form_id]: submission_id } },
        { upsert: true })),
      TE.map(_ => res.redirect(`/form/${encodeURIComponent(form_id)}`)),
      TE.mapLeft(err => {
        console.error("Failed to get signing token for PDF", err);
        res.redirect(`/form/${encodeURIComponent(form_id)}`);
      })
    );
  } else {
    return TE.left(res.status(403).end());
  }
});

export default handler.apply();