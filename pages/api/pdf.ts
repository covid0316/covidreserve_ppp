import { FpConnect, authenticate } from '../../lib/tsNextConnect';
import { DocspringClient, FpDocspringClient } from '../../lib/docspringClient';
import * as TE from 'fp-ts/lib/TaskEither';
import { pipe } from 'fp-ts/lib/pipeable';
import middleware from '../../middlewares/middleware';
import { PdfRequest, PdfResponse } from './data/pdfTypes';

const handler = new FpConnect();

const templateId: string = process.env.FORM_TEMPLATE_5 as string;

const client = new FpDocspringClient(new DocspringClient(
  process.env.DOCSPRING_API_TOKEN as string,
  process.env.DOCSPRING_API_TOKEN_SECRET as string
));

handler.use(middleware);
handler.post<PdfRequest, PdfResponse>((req, res) => {
  const pdfRequest = req.body;
  return pipe(
    TE.fromEither(authenticate(req)),
    TE.chain(_ => client.generatePdf(templateId, pdfRequest)),
    TE.map(result => res.json(result)),
    TE.mapLeft(err => {
      console.error("Failed to generate PDF.", err);
      res.status(500)
        .json({
          message: "Failed to generate PDF",
          code: 500
        });
    })
  );
});

export default handler.apply();
