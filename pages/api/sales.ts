import nextConnect from 'next-connect';
import middleware from '../../middlewares/middleware';
import sgMail from '@sendgrid/mail';

const handler = nextConnect();
handler.use(middleware);
sgMail.setApiKey(process.env.SENDGRID_API_KEY!);

handler.post(async (req, res) => {
  try {
    const { firstname, lastname, email, phone } = req.body;
    const salesMsg = {
      to: 'covidreserve@gmail.com',
      bcc: 'info@covidreserve.com',
      from: process.env.EMAIL_FROM!,
      subject: 'Covid Reserve Sign Up',
      html: `
        <div>
          <p>Name: ${firstname} ${lastname}</p>
          <p>Email: ${email}</p>
          <p>Phone: ${phone}</p>
        </div>
    `
    };
    const userMsg = {
      to: email,
      from: process.env.EMAIL_FROM!,
      templateId: 'd-195a79f73a14484b8e7c28beda1655ec', // Welcome template
      dynamicTemplateData: { firstName: firstname },
    };

    // @todo: investigate sendMultiple or use a single await
    await sgMail.send(salesMsg);
    await sgMail.send(userMsg);
    res.json({ message: 'An email has been sent to your inbox.' });
  } catch (error) {
    console.error(
      'An error occurred processing a password reset request.',
      error
    );
    res.json({
      ok: false,
      message:
        error.userFacingMessage ||
        'Sorry, something went wrong. Please try again.'
    });
  }
});

export default handler.apply.bind(handler);
