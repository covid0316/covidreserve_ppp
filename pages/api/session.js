import nextConnect from 'next-connect';
import middleware from '../../middlewares/middleware';

const handler = nextConnect();

handler.use(middleware);

handler.get((req, res) => {
  if (req.user) {
    const {
      _id, name, email, bio, profilePicture, emailVerified,
    } = req.user;
    return res.json({
      data: {
        isLoggedIn: true,
        user: {
          _id,
          name,
          email,
          bio,
          profilePicture,
          emailVerified,
        },
      },
    });
  }
  return res.json({
    data: {
      isLoggedIn: false,
      user: {},
    },
  });
});

handler.delete((req, res) => {
  req.logOut();
  res.json({
    ok: true,
    message: 'You have been logged out.',
  });
});

export default handler.apply.bind(handler);
