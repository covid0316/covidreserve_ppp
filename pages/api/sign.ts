import { pipe } from 'fp-ts/lib/pipeable';
import { FpConnect, authenticate } from '../../lib/tsNextConnect';
import { DocspringClient, FpDocspringClient, DataRequestToken } from '../../lib/docspringClient';
import * as TE from 'fp-ts/lib/TaskEither';
import * as E from 'fp-ts/lib/Either';
import middleware from '../../middlewares/middleware';
import { fromPromise } from '../../lib/util';
import { flattenListForm } from '../../lib/storage';
import { PdfRequest, Form413D } from './data/pdfTypes';

type SignatureRequest = {
  formId: string
};

type Error = {
  message: string,
  code: number
}

const template = {
  '413D': process.env.FORM_TEMPLATE_413D,
  '5': process.env.FORM_TEMPLATE_5,
  '2202': process.env.FORM_TEMPLATE_2202
}

const client = new FpDocspringClient(new DocspringClient(
  process.env.DOCSPRING_API_TOKEN as string,
  process.env.DOCSPRING_API_TOKEN_SECRET as string
));

const handler = new FpConnect();

handler.useMiddleware(middleware);
handler.get<void, DataRequestToken | Error>((req, res) => {
  const formId = req.query.formId;
  const formName = req.query.formName;
  const db = req.db!;
  
  return pipe(
    TE.fromEither<any, undefined>(authenticate(req)),
    TE.chain(_ => fromPromise(
      db.collection(`form-${formId}`)
        .findOne({ _id: req.user._id }))),
    TE.chain(formData =>
      pipe(
        TE.fromEither(toPdfRequest(formName ? formData[formName] : formData, formId)),
        TE.chain(data => client.generateDataRequest(
          template[formId],
          req.user.email,
          data)),
        TE.chain(dataRequestId => client.createDataRequestToken(dataRequestId.id)))
      ),
    TE.map(result => res.json(result)),
    TE.mapLeft(err => {
      if (err.notAuthorized) {
        res.status(401).end();
      } else {
        console.error("Failed to get signing token for PDF", err);
        res.status(500).json({
          message: "Failed to sign PDF",
          code: 500
        });
      }
    })
  );
});

function toPdfRequest(data: any, formId: string): E.Either<any, PdfRequest> {
  switch (formId) {
    case '5':
      return E.right({} as PdfRequest);
      break;
    case '413D':
      return E.right({} as PdfRequest);
      break;
  }
  return E.left("Invalid form type");
}

export default handler.apply();