const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const router = express.Router();
const AWS = require('aws-sdk');

app.use('/', function (req, res, next) {
res.setHeader('Access-Control-Allow-Origin', '*');
res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Max-Age');
next();
});
const s3 = new AWS.S3({
  accessKeyId: "your access key id", // pass your accessKeyId
  secretAccessKey: "your secretAccessKey" // pass your secretAccessKey
});

const routerPost = router.post('/',(req,res,next)=>{
    const base64String = req.body.base64String;
    let buffer = new Buffer.from(base64String.toString().split(';base64,')[1], 'base64');
         const params = {
             Bucket: 'put your bucket name', // pass your bucket name
             Key:req.body.name , 
             Body: buffer
         };
         s3.upload(params, function(s3Err, data) {
             if (s3Err) {
              res.status(500).json({
                    ok: false,
                    message: s3Err || 'Sorry, something went wrong. Please try again.',
                  });
             }
             res.status(201).json({
                ok: true,
                message: `File uploaded successfully at ${data.Location}`,
              });
         });

});

app.use(bodyParser.json());
app.use('/uploadFile',routerPost);

module.exports = app;