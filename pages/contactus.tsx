import React, {useContext, useState} from 'react';
import {UserContext} from '../components/UserContext';
import Layout from '../components/homeLayout';
import Footer from "./Footer";
import {Field, Form, Formik} from "formik";
import {Logo} from "../components/Icons";
import {ContactUsBox} from '../components/contactPageStaticImages'
import emailjs from "emailjs-com";
import fetchSwal from '../lib/fetchSwal';

type User = {
    email: string,
    password: string
}

const card = {
    border: "1px solid",
    borderColor: "#116DC1",
    borderRadius: "8px"
};
const cardHeader = {
    backgroundColor: "#116DC1",
    color: "white"
};

const formControl = {
                    display: 'block',
                    width: '100%',
                    padding: '0.375rem 0.75rem',
                    fontSize: '1rem',
                    fontWeight: 400,
                    lineHeight: 1.5,
                    color: '#495057',
                    backgroundColor: '#fff',
                    backgroundClip: 'padding-box',
                    border: '1px solid #ced4da',
                    borderRadius: '0.25rem',
                    transition: 'border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out',
                    boxSizing: 'border-box' ,
                }

const initialData = {
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    message: "",
}
const LoginPage = () => {
    const {dispatch} = useContext(UserContext);
    const [showmsg, setShowmsg] = useState(false)
    const [invalid, setError] = useState("");

    const submit1 = (values) => {
        const data = {
            name: values.firstName + " " + values.lastName,
            phone: values.phone,
            email: values.email,
            message: values.message,
        }
        //TODO- change to send-grid.
        // Please replace to this for dev testing: emailjs.send('gmail', 'template_dvUwnz9H', data, 'user_CVskhUKHgJOt00jRfLAPm')
        emailjs.send('gmail', 'template_m1FDH8fF', data, 'user_Hbbw137RhE5oH8b81QrxM')
        .then((result) => {
            if (result.text === "OK") {
                setShowmsg(true)
            }
        }, (error) => { console.log(error.text);});
    }

    const submit = (values, resetForm) => {
        setError("");
        var text = "Please provide proper ";

        if(values.firstName == ""){
            text = text + "Name - ";
        }

        var phon = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

        if(!phon.test(values.phone)){
            text = text + "Phone - ";
        }

        var re = /\S+@\S+\.\S+/;

        if(!re.test(values.email)){
            text = text + "Email";
        }

        if(text != "Please provide proper "){
            setError(text);
            return;
        }

        fetchSwal
            .post('/api/contact', values)
            .then((data) => {
                if (data.ok !== false) {
                    // alert("We'll get back to you shortly.");
                    setShowmsg(true);
                    resetForm({});
                }
            });
    }

    return (
        <Layout>
            <div className="container-fluid  p-0">

                                <div className="hero ">

                                    <div className=" flex-center">

                                        <div className="hero-message">

                                            <h1 className="hero-title">Contact us about </h1>

                                            <h2 className="hero-sub-title">CovidReserve's software</h2>

                                        </div>

                                    </div>

                                    <div className="not-hero flex-center bg-infos">

                                        <div className="not-hero-message">

                                            <h4 className="description">We'd love to show you how CovidReserve can help streamline</h4>

                                            <h4 className="description">your Covid-19 Disaster Relife loan application process.</h4>

                                        </div>

                                    </div>

                                </div>




            <div className="container-custom mt-8">
                <div className="row">

                    <div className="col-lg-5 text-center">
                        {ContactUsBox}
                    </div>

                        <div className=" col-lg-1"></div>

                    <div className="col-lg-6">

                        <Formik
                            initialValues={initialData}
                            enableReinitialize={true}
                            validate={values => {
                                const errors = {};
                                return errors;
                            }}
                            onSubmit={(values, {setSubmitting, resetForm}) => {
                                setTimeout(() => {
                                    setSubmitting(false);
                                    submit(values,resetForm)
                                }, 1);
                            }}
                        >
                            {({
                                  values,
                                  touched,
                                  errors,
                                  dirty,
                                  isSubmitting,
                                  handleChange,
                                  handleBlur,
                                  handleSubmit,
                                  handleReset,
                              }) => (
                                <Form>


                                    <div className="card" style={card}>
                                        <div className="card-header" style={cardHeader}>

                                            <h1 className="font-weight-bold text-center">{Logo}</h1>


                                        </div>

                                        {showmsg ? (<div style={{backgroundColor: 'darkseagreen'}} className="alert alert-success" role="alert">

                                            Thanks for contacting us. We will get in touch with you soon.

                                        </div>) : ""}


                                        <div className="card-body contact_frm">
                                            <blockquote className="blockquote mb-0">
                              <h2 className="cnt_our_tm text-center p-2">Contact our team</h2>

                                                <div className="row">
                                                    <div className="col-sm-6">
                                                        <div className="form-group">
                                                            <label htmlFor="exampleInputEmail1">First Name</label>
                                                            <Field name="firstName" style={formControl} className="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6">
                                                        <div className="form-group">
                                                            <label htmlFor="exampleInputEmail1">Last Name</label>
                                                            < Field name="lastName"  style={formControl} className="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Email Address</label>
                                                    <Field name="email" className="form-control"  style={formControl}/>
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Phone No </label>
                                                    <Field name="phone"  style={formControl} className="form-control "/>
                                                </div>


                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Message </label>
                                                    <Field name="message"  style={formControl} className="form-control" component={renderTextField}/>
                                                </div>
                                                <p style={{fontSize: 13,color: 'red',position: 'absolute',bottom:0,left: 3}}>
                                                    {invalid}
                                                </p>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        width: '100%',
                                                        paddingBottom: '18px'
                                                    }}
                                                >


                                                    <button style={{margin: '0px 40% 0 auto', width:'150px'}} className='nextbutton btn btn-lg'
                                                            type="submit"
                                                            disabled={isSubmitting}>
                                                        Submit
                                                    </button>
                                                </div>
                                            </blockquote>
                                        </div>
                                    </div>


                                </Form>
                            )}
                        </Formik>


                    </div>
                    </div>


                </div>


                <div className="row" style={{marginTop: "100px"}}>
                </div>

                <Footer/>
            </div>

            <style jsx>
                {`
                .hero-tag-one {
                color: #FFFFFF;
                position: absolute;
                left: 10%;
                font-style: normal;
                font-weight: bold;
                margin-top: 0%;
                font-family: Lato;
                font-style: normal;
                font-weight: bold;
                font-size: 48px;
                line-height: 142%;

                /* or 68px */
            }
            .form-control {
                    display: block;
                    width: 100%;
                    height: calc(1.5em + 0.75rem + 2px);
                    padding: 0.375rem 0.75rem;
                    font-size: 1rem;
                    font-weight: 400;
                    line-height: 1.5;
                    color: #495057;
                    background-color: #fff;
                    background-clip: padding-box;
                    border: 1px solid #ced4da;
                    border-radius: 0.25rem;
                    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                }

            .form-group input {
                                    border: 1px solid !important;
                                }
            .cnt_our_tm {
                font-weight: 400;
            }
            .nextbutton, .completebutton {
                background-color: #1382E9;
                color: white;
                font-family: 'Lato';
            }

            svg {
                overflow: hidden;
                vertical-align: middle;
            }
            @media (min-width: 992px) {
            .col-lg-4 {
                flex: 0 0 33.333333%;
                max-width: 33.333333%;
            }
             .col-lg-6 {
                flex: 0 0 50%;
                max-width: 50%;
             }
            .col-lg-2 {
                flex: 0 0 16.666667%;
                max-width: 16.666667%;
            }}
            .card-header:first-child {
                border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
            }
            .card {
                    position: relative;
                    display: flex;
                    flex-direction: column;
                    min-width: 0;
                    word-wrap: break-word;
                    background-color: #fff;
                    background-clip: border-box;
                    border-radius: 0.25rem;
                }
            .card-header {
                padding: 0.75rem 1.25rem;
                margin-bottom: 0;
                background-color: #1382e9 !important;
                border-bottom: 1px solid rgba(0, 0, 0, 0.125);
            }
            .card-body {
                flex: 1 1 auto;
                min-height: 1px;
                padding: 1.25rem 3rem;
            }


            .contact-us-box {
                display: flex;
                flex-direction: column;
                padding: 60px;

                position: absolute;
                width: 435px;
                height: 378px;
                left: 126px;
                top: 722px;

                /* White */
                background: #FFFFFF;

                /* primary-blue */
                border: 1px solid #1382E9;
                box-sizing: border-box;
                border-radius: 5px;

            }

            .hero-img-wrapper{

            }

             .contactUsMessage{
                display: flex;
                flex-direction: column;
                padding: 8px 16px;

                position: static;
                width: 416px;
                height: 131px;
                left: 0px;
                top: 31px;

                background: #FFFFFF;
                border: 1px solid #65696E;
                box-sizing: border-box;
                border-radius: 5px;

                /* Inside Auto Layout */
                flex: none;
                order: 1;
                align-self: flex-start;
                margin: 0px 8px;

            }

            .phoneNo {
                position: static;
                left: 0%;
                right: 0%;
                top: 0%;
                bottom: 0%;

                font-family: Lato;
                font-style: normal;
                font-weight: bold;
                font-size: 48px;
                line-height: 100%;

                /* identical to box height, or 48px */

                /* primary-blue */
                color: #1382E9;

                /* Inside Auto Layout */
                flex: none;
                order: 0;
                align-self: center;
                margin: 0px 8px;

            }
            .frm_contact {
               font-size: 28px;
                font-weight: 700;
                margin: 0;
                padding: 8px 0;
            }

            .hero-tag-two {
                color: #FFFFFF;
                position: absolute;
                left: 10%;
                font-style: normal;
                font-weight: bold;
                font-size: 48px;
                margin-top: 5%;
            }.desc{
                color: #FFFFFF;
                position: absolute;
                left: 10%;
                font-style: normal;
                font-weight: bold;
                font-size: 28px;
                margin-top: 10%;
                font-family: Lato;
                font-style: normal;
                font-weight: bold;
                font-size: 20px;
                line-height: 142%;
            }.desc-two{
                color: #FFFFFF;
                position: absolute;
                left: 10%;
                font-style: normal;
                font-weight: bold;
                font-size: 28px;
                margin-top: 12%;
                font-family: Lato;
                font-style: normal;
                font-weight: bold;
                font-size: 20px;
                line-height: 142%;
            }

                .hero-img {
                width: 100%;
                min-width: 1824px;
            }

            .text-box-contactus {
                border: 1px solid #ced4da
            }


            .hero {

                                    width: 100%;

                                    height: 100%;

                                    min-width: 100%;

                                    min-height: 100%;

                                    position: relative;

                                    padding: 8em 0;

                                }



                                .hero::before {

                                    background-image: url(/img/ContactUs.jpg);

                                    background-size: cover;

                                    background-position-x: 91%;

                                    content: "";

                                    display: block;

                                    position: absolute;

                                    top: 0;

                                    left: 0;

                                    width: 100%;

                                    height: 100%;

                                    z-index: -2;

                                    opacity: 0.9;

                                }



                                .hero::after {

                                    content: "";

                                    display: block;

                                    position: absolute;

                                    top: 0px;

                                    left: 0px;

                                    width: 100%;

                                    height: 100%;

                                    z-index: -1;

                                    opacity: 0.9;

                                }



                                .flex-center {

                                    display: flex;

                                    flex-direction: column;

                                    justify-content: center;

                                    align-content: center;

                                }



                                .not-hero {

                                    width: 100vw;

                                    height: 10em;

                                }



                                .bg-infos {

                                background-color: transparent !important;

                                    color: #fff;

                                }



                                .not-hero-message {

                                    color: #FFFFFF;

                                    min-width: 50%;

                                    min-height: 7em;

                                }
                                .form-group input {
                                    margin-left: 0 !important;
                                    border: 1px solid;
                                }
                                *, *::before, *::after {
                                    box-sizing: border-box;
                                }
                                .row {
                                    display: flex;
                                    flex-wrap: wrap;
                                    margin-right: -15px;
                                    margin-left: -15px;
                                }
                                .form-group input {
                  margin-left: 0 !important;
                }
                .contact_frm {
                    padding: 1rem 3rem;
                }
                .container-custom {
                    width: 80%;
                    margin: auto;
                }
                .mt-8 {
                  padding-top: 6rem;
                }
                .row {
                                display: flex;
                                flex-wrap: wrap;
                                margin-right: -15px;
                                margin-left: -15px;
                            }

                .form-group {
                    margin-bottom: 1rem;
                }
                label {
                    display: inline-block;
                    margin-bottom: 0.5rem;
                }
                body {
                    margin: 0;
                    /*font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";*/
                    font-size: 1rem;
                    font-weight: 400;
                    line-height: 1.5;
                    color: #212529;
                    text-align: left;
                    background-color: #fff;
                }
                *, *::before, *::after {
                                    box-sizing: border-box !important;
                                }
                textarea {
                    overflow: auto;
                    resize: vertical;
                }
                input, button, select, optgroup, textarea {
                    margin: 0;
                    font-family: inherit;
                    font-size: inherit;
                    line-height: inherit;
                }
                .text-center {
                    text-align: center;
                }
                .blockquote {
                    margin-bottom: 1rem;
                    font-size: 1.25rem;
                }
                .img-fluid {
                    max-width: 100%;
                    height: auto;
                }
                .card-body {
                    flex: 1 1 auto;
                    min-height: 1px;
                    padding: 1.25rem;
                }
                button {
                    border: none;
                }
                .justify-content-center {
                    justify-content: center !important;
                }
                article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
                    display: block;
                }



            .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
                position: relative;
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
            }
             .signup-width{
                width: 80%;
              }


             svg {
                overflow: hidden;
                vertical-align: middle;
            }



                                .hero-message {

                                    color: #fff;

                                    // text-shadow: #343a40 2px 2px;

                                    min-width: 100%;

                                    min-height: 12em;

                                    position: relative;

                                }



                                 input.form-control {
                                        border: 1px solid #7c7a83;
                                    }
                                .hero-message::before {

                                    content: "";

                                    display: block;

                                    position: absolute;

                                    margin-left: 0;

                                    min-width: 100%;

                                    min-height: 12em;

                                    z-index: -1;

                                    opacity: 0.9;

                                }



                                .hero-title,

                                .hero-sub-title {

                                    width: 100%;

                                    display: block;

                                    text-align: left;

                                    padding-left: 2%;

                                    font-weight:600;
                                    color: #FFFFFF;
                                    font-size: 50px;
                                    margin: 0;

                                }



                                .hero-title {

                                    margin: 1% 0;

                                    color: #FFFFFF;

                                    font-weight:600;
                                    font-size: 50px;

                                }

                                .description {

                                    margin: 4px 0;

                                    color: #FFFFFF;

                                    padding-left:2%;

                                }



                                @media(min-width:1024px) {

                                    .hero-message {

                                        min-width: 50%;

                                        min-height: 5em;

                                    }

                                    .hero-message::before {

                                        margin-left: 25%;

                                        min-width: 50%;

                                        min-height: 12em;

                                    }

                                }



                                @media (min-width: 320px) {

                                    .hero::before {

                                        background-image: url(/img/ContactUs.jpg);

                                    }

                                }



                                @media (min-width: 460px) {

                                    .hero::before {

                                        background-image: url(/img/ContactUs.jpg);

                                    }

                                }



                                @media (min-width: 720px) {

                                    .hero::before {

                                        background-image: url(/img/ContactUs.jpg);

                                    }

                                }



                                @media (min-width: 980px) {

                                    .hero::before {

                                        background-image: url(/img/ContactUs.jpg);

                                    }

                                }
                                @media (min-width: 992px) {
                                    .col-lg-5 {
                                        flex: 0 0 41.666667%;
                                        max-width: 41.666667%;
                                    }
                                .col-lg-1 {
                                        flex: 0 0 8.333333%;
                                        max-width: 8.333333%;
                                    }
                                .col-lg-6 {
                                    flex: 0 0 50%;
                                    max-width: 50%;
                                }
                            .col-lg-4 {
                                flex: 0 0 33.333333%;
                                max-width: 33.333333%;
                            }
                            .col-lg-3 {
                                    flex: 0 0 25%;
                                    max-width: 25%;
                                }
                            .col-lg-3 {
                                flex: 0 0 25%;
                                max-width: 25%;
                            }}
                             @media (min-width: 576px) {
                            .col-sm-12 {
                                flex: 0 0 100%;
                                max-width: 100%;
                            }}



                                @media (min-width: 1240px) {

                                    .hero::before {

                                        background-image: url(/img/ContactUs.jpg);

                                    }

                                }



                                @media (min-width: 1500px) {

                                    .hero::before {

                                        background-image: url(/img/ContactUs.jpg);

                                    }

                                }



                                @media (min-width: 1760px) {

                                    .hero::before {

                                        background-image: url(/img/ContactUs.jpg);

                                    }

                                }
                                @media (max-width: 567px) {
                                    .blockquote {
                                    margin: 0;
                                } .hero-title, .hero-sub-title {
                                    font-size: 30px;
                                }
                                .not-hero-message h4 {
                                    font-size: 20px;
                            }}
                            .form-group input {
                                    border: 1px solid !important;
                                }


            @media (min-width: 992px) {
            .col-lg-4 {
                flex: 0 0 33.333333%;
                max-width: 33.333333%;
            }
             .col-lg-6 {
                flex: 0 0 50%;
                max-width: 50%;
             }
            .col-lg-2 {
                flex: 0 0 16.666667%;
                max-width: 16.666667%;
            }
          .col-lg-3 {
    flex: 0 0 25%;
    max-width: 25%;
}}
@media (max-width: 567px) {
  .signup-width{
      width: 95%;
  }
  .col-sm-12 {
    flex: 0 0 100%;
    max-width: 100%;
  }
}
@media (max-width: 700px) {
  .mobile-form-deny{
    background-color: white;
    height: 100%;
    width: 100%;
    position: fixed;
    bottom: 0px;
    text-align: center;
  }
  .mobile-form-deny-text{
    position: relative;
    top: 50%;
    transform: translateY(-50%);
  }
}
@media (min-width: 701px) {
  .mobile-form-allow-text{
    color: transparent;
  }
}



@media (min-width: 576px) {
.col-sm-6 {
    flex: 0 0 50%;
    max-width: 50%;
}}

@media screen and (min-width: 1074px) {
  .signup-width{
    width: 38%;
  }
}
.form-control {
                    display: block;
                    width: 100%;
                    height: calc(1.5em + 0.75rem + 2px);
                    padding: 0.375rem 0.75rem;
                    font-size: 1rem;
                    font-weight: 400;
                    line-height: 1.5;
                    color: #495057;
                    background-color: #fff;
                    background-clip: padding-box;
                    border: 1px solid #ced4da;
                    border-radius: 0.25rem;
                    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                }







            `}

            </style>
        </Layout>
    );
};

const renderTextField = ({field, label, type}) => (
    <div>
        <textarea rows="4" cols="72" {...field} type={type} placeholder={label} style={{border: '1px solid #ced4da', boxSizing: 'border-box'}}/>
    </div>
)

export default LoginPage;
