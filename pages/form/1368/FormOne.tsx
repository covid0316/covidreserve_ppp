import Layout from '../../../components/layout';
import {Field, FieldArray, Form, Formik, FastField} from "formik";
import React, {useEffect, useState} from "react";
import * as storage from "../../../lib/storage";
import _ from "lodash";

const tableCss = {
    margin: "auto",
    width: "100%",
    borderColor: 'transparent',
    padding: '12px 0px 12px 0px',
    textAlign: 'center'
}
const checkbox = {
    top: "0.1rem !important",
    left: "-2rem !important",
    width: "1.25rem !important",
    height: "1.25rem !important"
}

const styles = {
     tableBordered:{
        border: '1px solid rgb(64, 63, 63)' ,
        borderSpacing: 0 ,
    } as React.CSSProperties,
    thHead:{
        borderBottom: '0.5px solid rgb(64, 63, 63)' ,
    } as React.CSSProperties,
    tableHeader:{
        alignItems: "center",
        display: "flex",
        flexDirection: "column",
        fontSize: 12,
        verticalAlign: 'text-top',
        border: '0.5px solid #000' ,
        borderColor:'#1F2933',

    } as React.CSSProperties,
    thFont:{
        fontSize: 12,
        verticalAlign: 'text-top',
        textAlign: 'center',
        border: '0.5px solid rgb(64, 63, 63)' ,
        borderColor:'#1F2933',
        paddingLeft: 11,
         paddingRight: 11,
    } as React.CSSProperties,
    thInput:{
        margin: "auto",
        width: "72%",
        marginTop: 8,
        marginBottom: 8,
        fontSize: 13,
        textAlign: 'center',
        padding: 5,
        borderColor: '#1F2933'
    } as React.CSSProperties,
    formTd:{
        borderColor: '#1F2933',
        border: '0.5px solid rgb(64, 63, 63)' ,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 11,
         paddingRight: 11,
        verticalAlign: 'middle',
    } as React.CSSProperties,
}

const formOne = {
    fiscal_year_first: "",
    fiscal_year_second: "",
    fiscal_year_third: "",
    current_year_to_date: "",
    fiscal_year_data: [
        {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }
    ],
    fiscal_year_one_total: "",
    fiscal_year_two_total: "",
    fiscal_year_three_total: "",
    in_total: ""
};
export default ({handleTabOne,formFin,handleForm}) => {
    const [savedData, setSavedData] = useState(formFin);
    const [loading, setLoading] = useState(true);


    useEffect(() => {
        async function fetchData() {
            setLoading(true);
            const data = await storage.FormStorageHelper.get(1368);
            if (Object.keys(data).length !== 0) {
                setSavedData(data);

            };
            setLoading(false);
        }
        fetchData();
    }, []);

    if (loading) {
        return <div />;
    }

    return (
        <Formik
            initialValues={savedData}
            enableReinitialize={true}
            onSubmit={(values, {setSubmitting}) => {
                setTimeout(() => {
                    setSubmitting(false);
                    // storage.FormStorageHelper.set(values, 1368, 1);
                    // handleTabOne(2)
                    handleForm(savedData);
                }, 1);
            }}
            render={({values, handleChange}) => (
                <Form>
                    <p style={{fontSize: "20px", marginBottom: "14px"}}>Task 1</p>
                    <h1>Please provide monthly sales figures (you may estimate if actual figures are not available)
                        beginning 3 years prior to the disaster (if available) and continuing through the most recent
                        month available</h1>
                    <p style={{fontSize: "20px", marginBottom: "14px"}}>Please check the box in column est for any
                        estimates any provide</p>


                    <div className={"table-responsive"}>
                        <table className="table  table-bordered " style={styles.tableBordered}>
                            <thead style={styles.thHead}>
                            <tr>
                                <th style={styles.thFont} scope="col">Month</th>
                                <th style={styles.tableHeader} scope="col">Fiscal year&nbsp;<br/>
                                    <Field  placeholder="YYYY" name="fiscal_year_first"
                                            onChange={(e) => {
                                                if(!isNaN(e.target.value))
                                                    setSavedData({...savedData,fiscal_year_first: e.target.value});
                                            }} value={savedData.fiscal_year_first}
                                            style={styles.thInput}/>
                                </th>
                                <th style={styles.thFont} scope="col">est</th>
                                <th style={styles.tableHeader} scope="col">Fiscal year&nbsp;<br/>
                                    <Field  placeholder="YYYY" name="fiscal_year_second"
                                            onChange={(e) => {
                                                if(!isNaN(e.target.value))
                                                    setSavedData({...savedData,fiscal_year_second: e.target.value});
                                            }} value={savedData.fiscal_year_second}
                                            style={styles.thInput}/>
                                </th>
                                <th style={styles.thFont} scope="col">est</th>
                                <th style={styles.tableHeader} scope="col">Fiscal year&nbsp;<br/>
                                    <Field  placeholder="YYYY" name="fiscal_year_third"
                                            onChange={(e) => {
                                                if(!isNaN(e.target.value))
                                                    setSavedData({...savedData,fiscal_year_third: e.target.value});
                                            }} value={savedData.fiscal_year_third}
                                            style={styles.thInput}/>
                                </th>
                                <th style={styles.thFont} scope="col">est</th>
                                <th style={styles.tableHeader} scope="col">Current Year to date&nbsp;<br/>
                                    <Field  placeholder="YYYY" name="current_year_to_date"
                                            onChange={(e) => {
                                                if(!isNaN(e.target.value))
                                                    setSavedData({...savedData,current_year_to_date: e.target.value});
                                            }} value={savedData.current_year_to_date}
                                            style={styles.thInput}/>
                                </th>
                                <th style={styles.thFont} scope="col">est</th>
                            </tr>
                            </thead>
                            <tbody>


                            <FieldArray
                                name="fiscal_year_data"
                                render={arrayHelpers => (
                                    <>
                                        {values.fiscal_year_data.map((friend, index) => (
                                            <tr key={index}>
                                                <td style={styles.formTd}>
                                                    <Field name={`fiscal_year_data[${index}].month`}
                                                           style={tableCss}
                                                           onChange={(e) => {
                                                                savedData.fiscal_year_data[index].month = e.target.value;
                                                                setSavedData({...savedData,fiscal_year_data: savedData.fiscal_year_data});
                                                            }} value={savedData.fiscal_year_data[index].month}
                                                           placeholder="Month"/>
                                                </td>
                                                <td style={styles.formTd}>
                                                    <Field name={`fiscal_year_data.${index}.fiscal_year_one`}
                                                           style={tableCss}
                                                           type="text"
                                                           onChange={(e) => {
                                                                if(!isNaN(e.target.value)){
                                                                    savedData.fiscal_year_data[index].fiscal_year_one = e.target.value;
                                                                    setSavedData({...savedData,fiscal_year_data: savedData.fiscal_year_data});
                                                                }
                                                            }} value={savedData.fiscal_year_data[index].fiscal_year_one}
                                                           placeholder="$xx.xx"/>
                                                </td>
                                                <td style={styles.formTd}>
                                                    <Field type={"checkbox"}
                                                           name={`fiscal_year_data[${index}].estimate_one`}
                                                           onChange={(e) => {
                                                                savedData.fiscal_year_data[index].estimate_one = e.target.checked;
                                                                setSavedData({...savedData,fiscal_year_data: savedData.fiscal_year_data});
                                                            }} checked={savedData.fiscal_year_data[index].estimate_one}
                                                           style={checkbox}/>
                                                </td>
                                                <td style={styles.formTd}>
                                                    <Field name={`fiscal_year_data.${index}.fiscal_year_two`}
                                                           style={tableCss}
                                                           placeholder="$xx.xx"
                                                           onChange={(e) => {
                                                                if(!isNaN(e.target.value)){
                                                                    savedData.fiscal_year_data[index].fiscal_year_two = e.target.value;
                                                                    setSavedData({...savedData,fiscal_year_data: savedData.fiscal_year_data});
                                                                }
                                                            }} value={savedData.fiscal_year_data[index].fiscal_year_two}
                                                           type="text"/>
                                                </td>
                                                <td style={styles.formTd}>
                                                    <Field type={"checkbox"}
                                                           name={`fiscal_year_data[${index}].estimate_two`}
                                                           onChange={(e) => {
                                                                savedData.fiscal_year_data[index].estimate_two = e.target.checked;
                                                                setSavedData({...savedData,fiscal_year_data: savedData.fiscal_year_data});
                                                            }} checked={savedData.fiscal_year_data[index].estimate_two}
                                                           style={checkbox}/>
                                                </td>
                                                <td style={styles.formTd}>
                                                    <Field name={`fiscal_year_data.${index}.fiscal_year_three`}
                                                           style={tableCss}
                                                           placeholder="$xx.xx"
                                                           onChange={(e) => {
                                                                if(!isNaN(e.target.value)){
                                                                    savedData.fiscal_year_data[index].fiscal_year_three = e.target.value;
                                                                    setSavedData({...savedData,fiscal_year_data: savedData.fiscal_year_data});
                                                                }
                                                            }} value={savedData.fiscal_year_data[index].fiscal_year_three}
                                                           type="text"/>
                                                </td>
                                                <td style={styles.formTd}>
                                                    <Field type={"checkbox"}
                                                           name={`fiscal_year_data[${index}].estimate_three`}
                                                           onChange={(e) => {
                                                                savedData.fiscal_year_data[index].estimate_three = e.target.checked;
                                                                setSavedData({...savedData,fiscal_year_data: savedData.fiscal_year_data});
                                                            }} checked={savedData.fiscal_year_data[index].estimate_three}
                                                           style={checkbox}/>
                                                </td>
                                                <td style={styles.formTd}>
                                                    <Field name={`fiscal_year_data.${index}.current_year`}
                                                           style={tableCss}
                                                           placeholder="$xx.xx"
                                                           onChange={(e) => {
                                                                if(!isNaN(e.target.value)){
                                                                    savedData.fiscal_year_data[index].current_year = e.target.value;
                                                                    setSavedData({...savedData,fiscal_year_data: savedData.fiscal_year_data});
                                                                }
                                                            }} value={savedData.fiscal_year_data[index].current_year}
                                                           type="text"/>
                                                </td>
                                                <td style={styles.formTd}>
                                                    <Field type={"checkbox"}
                                                           name={`fiscal_year_data.${index}.estimate_current_year`}
                                                           onChange={(e) => {
                                                                savedData.fiscal_year_data[index].estimate_current_year = e.target.checked;
                                                                setSavedData({...savedData,fiscal_year_data: savedData.fiscal_year_data});
                                                            }} checked={savedData.fiscal_year_data[index].estimate_current_year}
                                                           style={checkbox}/>
                                                </td>
                                            </tr>
                                        ))}


                                        <tr>
                                            <td style={styles.formTd}>Totals*:
                                            </td>
                                            <td style={styles.formTd}>
                                                <Field name={`fiscal_year_one_total`}
                                                       type="text"
                                                       onChange={(e) => {
                                                            setSavedData({...savedData,fiscal_year_one_total: e.target.value});
                                                        }} value={savedData.fiscal_year_one_total}
                                                       style={tableCss}/>
                                            </td>
                                            <td style={styles.formTd}>
                                            </td>
                                            <td style={styles.formTd}>
                                                <Field name={`fiscal_year_two_total`}
                                                       type="text"
                                                       onChange={(e) => {
                                                            setSavedData({...savedData,fiscal_year_two_total: e.target.value});
                                                        }} value={savedData.fiscal_year_two_total}
                                                       style={tableCss}/>
                                            </td>
                                            <td style={styles.formTd}>
                                            </td>
                                            <td style={styles.formTd}>
                                                <Field name={`fiscal_year_three_total`}
                                                       type="text"
                                                       onChange={(e) => {
                                                            setSavedData({...savedData,fiscal_year_three_total: e.target.value});
                                                        }} value={savedData.fiscal_year_three_total}
                                                       style={tableCss}/>
                                            </td>
                                            <td style={styles.formTd}>
                                            </td>
                                            <td style={styles.formTd}>
                                                <Field name={`in_total`}
                                                       type="text"
                                                       onChange={(e) => {
                                                            setSavedData({...savedData,in_total: e.target.value});
                                                        }} value={savedData.in_total}
                                                       style={tableCss}/>
                                            </td>
                                            <td style={styles.formTd}>
                                            </td>
                                        </tr>
                                    </>
                                )}
                            />
                            </tbody>
                        </table>
                    </div>
                    <div style={{textAlign: 'right'}}>
                        <button style={{
                            margin: '25px 0px 0 auto',
                            borderColor: 'transparent',
                            padding: '10px 15px 10px 15px',
                            fontSize: 15
                        }}
                        className='nextbutton' type="submit">Next
                        </button>
                    </div>
                </Form>
            )}
        />

    );
};
