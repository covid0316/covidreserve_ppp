import { useState, useEffect } from 'react';
import { Formik, Form, Field } from 'formik';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Question } from '../../../../../components/QuestionTypes';
import Layout from '../../../../../components/layout';
import * as storage from '../../../../../lib/storage';
import DynamicQuestion from '../../../../../lib/renderQuestion';
import FormikWrapper from '../../../../../components/FormikWrapper';
import * as Yup from 'yup';


type Props = {};

const FormTitle = '413D';

const getInitialValues = (form: object, step: number) => {
  if (form && Object.keys(form).length) {
    return form;
  } else {
    return questions[step].reduce((obj, item: any) => {
      if (item.type === 'multi') {
        return { ...obj, [item.id]: [{}] };
      }

      if (item.type === 'none') {
        return { ...obj };
      }

      if (item.type === 'growingtable') {
        return { ...obj, [item.id]: [] };
      }

      if (item.type === 'date') {
        return { ...obj, [`${item.id}-month`]: '', [`${item.id}-day`]: '', [`${item.id}-year`]: '' }
      }

      if (item.other) {
        // eslint-disable-next-line no-param-reassign
        obj = { ...obj, [item.other]: false }; // @todo: re-assign is bad
      }

      if (item.fields) {
        return item.fields!.reduce(
          (obj2, item2) => ({ ...obj2, [item2.id]: false }),
          obj
        );
      }

      if (item.id) {
        return { ...obj, [item.id]: (item.type === 'text' || item.type === 'textarea' || item.type === 'currency') ? '' : false };
      }

      throw new Error('unexpected end')

    }, {});
  }
}


const _413D: NextPage<Props> = ({}) => {
  const router = useRouter();
  const formName = router.query.name as string;
  const step = parseInt(router.query.step as string, 10);
  const [loading, setLoading] = useState(true);
  const [formData, setFormData] = useState({});
  const submit = async (values, { setSubmitting }) => {
    setLoading(true);
    await storage.FormStorageHelper.set(values, FormTitle, step, formName);
    setSubmitting(false);
    router.replace(`/form/413D/attachment/${encodeURIComponent(formName)}/${step < 8 ? step + 1 : 'sign'}`);
  };

  const titles = [
    `Form 413D for: ${formName}`,
    'Assets: Source of Income',
    'Liabilities',
    'Stock and Bonds',
    'Mortgages on Real Estate',
    'Personal Property and Assets',
    'Unpaid Taxes',
    'Other Liabilities',
    'Section 8',
  ];
  
  useEffect(() => {
    setLoading(true);
    if (!isNaN(step) && formName) {
      storage.FormStorageHelper.get(FormTitle, step, formName)
        .then(data => {
          setFormData(getInitialValues(data, step));
          setLoading(false);
        });
    }
  }, [step, formName]);
  
  if (isNaN(step) || !formName ||!formData || loading) {
    return <Layout>Loading...</Layout>
  }

  return <Layout>
    <h2>{titles[step]}</h2>
    <div className="_413D-form">
      <FormikWrapper
        stepNum={step}
        storageKey={413}
        onSubmit={submit}
        validationSchema={yups[step]}
        initialValues={formData}
      >
        { ({isSubmitting, values, setFieldValue}) =>
          <Form>
            { questions[step].map((question) => {
              const renderedQuestion = (
                <DynamicQuestion
                  key={(question as any).id}
                  question={question}
                  setFieldValue={setFieldValue}
                  values={values}
                  num={null}
                  className='subquestion'
                />
              );
              if (question.renderData && question.renderData.smaller) {
                return (
                  <div className="smaller">
                    {renderedQuestion}
                  </div>
                )
              } else {
                return renderedQuestion;
              }
            })}
            <div className="buttons">
              <div className="back">
                <Link href={step === 0 ? `/form/413D` : `/form/413D/attachment/${encodeURIComponent(formName)}/${step - 1}`}>
                  {step === 0 ?
                    <a>Back to all 413D forms</a>
                  :
                    <a>Previous</a>
                  }
                </Link>
              </div>
              <div>
                <button type="submit">Next</button>
              </div>
            </div>
          </Form>
        }
      </FormikWrapper>
    </div>
    <style jsx>{`
      ._413D-form :global(.growingtable.horizontal .flex-grid .flex-grid-cell:first-child) {
        border-top: none;
        border-left: none;
        border-bottom: none;
        font-weight: bold;
      }
      ._413D-form :global(.growingtable.horizontal .flex-grid:first-child input) {
        font-weight: bold;
        text-align: center;
      }
      ._413D-form :global(.growingtable.vertical .flex-grid:first-child) {
        font-weight: bold;
      }
      ._413D-form :global(.smaller .subquestion) {
        font-size: 20px;
      }
      ._413D-form :global(.smaller input[type=text]) {
        width: 20%;
      }
      ._413D-form :global(.smaller .questionsection) {
        margin-left: 20px;
        padding-top: 0;
      }
      ._413D-form :global(.questionsection) {
        padding-left: 0 !important;
      }
      ._413D-form :global(.form-group) {
        margin-bottom: 0;
      }
      ._413D-form :global(.growingtable-wrapper),
      ._413D-form :global(.growingtable-wrapper-inner) {
        padding-left: 0 !important;
      }
      h2 {
        font-size: 48px;
        font-weight: normal;
        text-align: center;
      }
      .buttons {
        margin-top: 20px;
        display: flex;
        justify-content: space-between;
      }
      .buttons .back :global(a),
      .buttons .back :global(a:active),
      .buttons .back :global(a:visited) {
        padding: 0;
        padding: 12px 17px;
        font-size: 20px;
        cursor: pointer;
        color: #1F2933;
        background: #DCE6EF;
        display: block;
        border-radius: 5px;
      }
      button {
        padding: 0;
        background: #1382E9;
        width: 165px;
        height: 48px;
        font-size: 20px;
        cursor: pointer;
      }

    `}</style>
  </Layout>;
};

const yups: Array<Yup.Schema<any>> = [
  Yup.object().shape({
    '413-name': Yup.string().required('Name is required'),
    '413-address': Yup.string().required('Address is required'),
    '413-city-state-zip': Yup.string().required('Address is required'),
    '413-res-phone': Yup.string().required('Phone is required'),
    '413-applicant-business-name': Yup.string().required('Business name is required'),
    '413-business-phone': Yup.string().required('Phone is required'),
  }),
  Yup.object().shape({
    '413-assets-salary': Yup.number().typeError("Salary must be numeric").required('Salary is required'),
    '413-cash': Yup.number().typeError("Cash must be numeric").required('Cash is required'),
    '413-savings': Yup.number().typeError("Savings must be numeric").required('Savings is required'),
    '413-assets-net-investment': Yup.number().typeError("Income must be numeric").required('Income is required'),
    '413-assets-real-estate': Yup.number().typeError("Income must be numeric").required('Income is required'),
    '413-assets-other-income': Yup.number().typeError("Income must be numeric").required('Income is required'),
  }),
  Yup.object().shape({
    '413-accounts-payable': Yup.number().typeError("Accounts payable must be numeric").required('Accounts payable is required'),
    '413-install-auto': Yup.number().typeError("Installment account be numeric").required('Installment account is required'),
    '413-auto-mo': Yup.number().typeError("Payments must be numeric").required('Payments is required'),
    '413-install-other': Yup.number().typeError("Installment account must be numeric").required('Installment account is required'),
    '413-other-mo': Yup.number().typeError("Payments must be numeric").required('Payments is required'),
    '413-loan-life': Yup.number().typeError("Loan must be numeric").required('Loan is required'),
    '413-liabilites-endorser': Yup.number().typeError("Liability must be numeric").required('Liability is required'),
    '413-liabilities-legal': Yup.number().typeError("Liability must be numeric").required('Liability is required'),
    '413-liabilities-provision': Yup.number().typeError("Liability must be numeric").required('Liability is required'),
    '413-liabilities-other-special': Yup.number().typeError("Liability must be numeric").required('Liability is required'),
    '413-notes-payable': Yup.number().typeError("Notes payable must be numeric").required('Notes payable is required'),
  }),
  Yup.object().shape({
    '413-stocks': Yup.number().typeError("Stocks must be numeric").required('Stocks is required')
  }),
  Yup.object().shape({
    '413-mortgage': Yup.number().typeError("Mortgage must be numeric").required('Mortgage is required')
  }),
  Yup.object().shape({
    '413-ira': Yup.number().typeError("Retirement account must be numeric").required('Retirement account is required'),
    '413-a-n-r': Yup.number().typeError("Accounts receivable be numeric").required('Accounts receivable is required'),
    '413-automobiles': Yup.number().typeError("Value must be numeric").required('Value is required'),
    '413-other-pp': Yup.number().typeError("Property must be numeric").required('Property is required'),
    '413-other-assets': Yup.number().typeError("Other assets must be numeric").required('Other assets is required'),
  }),
  Yup.object().shape({
    '413-unpaid-tax': Yup.number().typeError("Unpaid taxes must be numeric").required('Unpaid taxes is required')
  }),
  Yup.object().shape({
    '413-other-liabilites': Yup.number().typeError("Liabilities must be numeric").required('Liabilities is required')
  }),
  Yup.object().shape({
    '413-life-insurance': Yup.number().typeError("Life insurance must be numeric").required('Life insurance is required')
  }),
];

const questions: Array<Question[]> = [
  [
    {
      id: '413-name',
      type: 'text',
      formno: [413],
      question: 'Name'
    },
    {
      id: '413-address',
      type: 'text',
      formno: [413],
      question: 'Residence Address'
    },
    {
      id: '413-city-state-zip',
      type: 'text',
      formno: [413],
      question: 'City, State and Zip Code'
    },
    {
      id: '413-res-phone',
      type: 'text',
      formno: [413],
      question: 'Residence Phone'
    },
    {
      id: '413-applicant-business-name',
      type: 'text',
      formno: [413],
      question: 'Business Name of Applicant / Borrower'
    },
    {
      id: '413-business-phone',
      type: 'text',
      formno: [413],
      question: 'Business Phone'
    }
  ],
  [
    {
      id: '413-assets-salary',
      type: 'currency',
      formno: [413],
      question: 'Salary'
    },
    {
      id: '413-cash',
      type: 'currency',
      formno: [413],
      question: 'Cash on Hand & in Banks'
    },
    {
      id: '413-savings',
      type: 'currency',
      formno: [413],
      question: 'Savings Accounts'
    },
    {
      id: '413-assets-net-investment',
      type: 'currency',
      formno: [413],
      question: 'Net Investment Income'
    },
    {
      id: '413-assets-real-estate',
      type: 'currency',
      formno: [413],
      question: 'Real Estate Income'
    },
    {
      id: '413-assets-other-income',
      type: 'currency',
      formno: [413],
      question: 'Other Income'
    },
    {
      id: '413-describe-other-1',
      type: 'textarea',
      formno: [413],
      question: 'Describe Other Income'
    }
  ],
  [
    {
      id: '413-accounts-payable',
      type: 'currency',
      formno: [413],
      question: 'Accounts Payable'
    },
    {
      id: '413-install-auto',
      type: 'currency',
      formno: [413],
      question: 'Installment Account (Auto)'
    },
    {
      id: '413-auto-mo',
      type: 'currency',
      formno: [413],
      question: 'Monthly Payments ($)',
      renderData: { smaller: true }
    },
    {
      id: '413-install-other',
      type: 'currency',
      formno: [413],
      question: 'Installment Account (Other)'
    },
    {
      id: '413-other-mo',
      type: 'currency',
      formno: [413],
      question: 'Monthly Payments ($)',
      renderData: { smaller: true }
    },
    {
      id: '413-loan-life',
      type: 'currency',
      formno: [413],
      question: 'Loan on Life Insurance'
    },
    {
      type: 'none',
      question: 'Contigent Liabilities',
      formno: [413],
    },
    {
      id: '413-liabilites-endorser',
      type: 'currency',
      formno: [413],
      question: 'As Endorser or Co-Maker',
      renderData: { smaller: true }
    },
    {
      id: '413-liabilities-legal',
      type: 'currency',
      formno: [413],
      question: 'Legal Claims & Judgements',
      renderData: { smaller: true }
    },
    {
      id: '413-liabilities-provision',
      type: 'currency',
      formno: [413],
      question: 'Provision for Federal Income Tax',
      renderData: { smaller: true }
    },
    {
      id: '413-liabilities-other-special',
      type: 'currency',
      formno: [413],
      question: 'Other Special Debt',
      renderData: { smaller: true }
    },
    {
      id: '413-notes-payable',
      type: 'currency',
      formno: [413],
      question: 'Notes Payable to Banks and Others'
    },
    {
      type: 'growingtable',
      question:
        'Describe Notes Payable to Banks and Others',
      columns: [
        'Name and Address of Note Holder(s)',
        'Original Balance',
        'Current Balance',
        'Payment Amount',
        'Frequency (monthly, etc)',
        'How Secured or Endorsed Type of Collateral'
      ],
      columnIds: [
        'noteholder-name-address',
        'notes-og-balance',
        'notes-cur-balance',
        'notes-payment-amount',
        'notes-freq',
        'notes-type'
      ],
      formno: [413],
      id: '413-notes-payable-table',
      gridSize: [2, 1, 1, 1, 2, 2],
      columnTypes: [
        'string',
        'number',
        'number',
        'number',
        'string',
        'string'
      ],
      rowTitle: 'row'
    }
  ],
  [
    {
      id: '413-stocks',
      type: 'currency',
      formno: [413],
      question: 'Stocks and Bonds'
    },
    {
      type: 'growingtable',
      question:
        '',
      columns: [
        'Number of Shares',
        'Name of Securities',
        'Cost',
        'Mkt Value / Quotation / Exchange',
        'Date of Quotation / Exchange',
        'How Secured or Endorsed Type of Collateral'
      ],
      columnIds: [
        'num-shares',
        'securities-name',
        'stock-cost',
        'market-value',
        'stock-date',
        'stock-total'
      ],
      formno: [413],
      id: '413-notes-payable-table',
      gridSize: [2, 1, 1, 1, 2, 2],
      columnTypes: [
        'number',
        'string',
        'number',
        'number',
        'date',
        'string'
      ],
      rowTitle: 'row'
    }
  ],
  [
    {
      id: '413-mortgage',
      type: 'currency',
      formno: [413],
      question: 'Mortgages on Real Estate'
    },
    {
      type: 'growingtable',
      question:
        '',
      direction: 'horizontal',
      columns: [
        '',
        'Type of Real Estate (e.g. Primary Residence, Other Residence, Rental Property Land, etc.)',
        'Address',
        'Date Purchased',
        'Original Cost',
        'Present Market Value',
        'Name & Address of Mortgage Holder',
        'Mortgage Account Number',
        'Mortgage Balance',
        'Amount of Payment per Mo. / Year',
        'Status of Mortgage'
      ],
      columnIds: [
        'property-name',
        'real-estate-type',
        'real-estate-address',
        'real-estate-date',
        'real-estate-og-cost',
        'real-estate-pmv',
        'real-estate-mortgage-name-address',
        'real-estate-mortgage-account',
        'real-estate-mortgage-balance',
        'real-estate-payment',
        'real-estate-status'
      ],
      formno: [413],
      id: '413-mortgage-table',
      gridSize: [], // not relevant for horizontal layout
      columnTypes: [
        'string',
        'string',
        'string',
        'date',
        'number',
        'number',
        'string',
        'string',
        'number',
        'number',
        'string'
      ],
      rowTitle: 'property',
      onAdd: (length) => ({[`property-name-${length-1}`]:  `Property ${String.fromCharCode(64 + length)}` })
    }
  ],
  [
    {
      id: '413-ira',
      type: 'currency',
      formno: [413],
      question: 'IRA or Other Retirement Account'
    },
    {
      id: '413-a-n-r',
      type: 'currency',
      formno: [413],
      question: 'Accounts and Notes Receivable'
    },
    {
      id: '413-automobiles',
      type: 'currency',
      formno: [413],
      question: 'Automobiles - Total Present Value'
    },
    {
      id: '413-other-pp',
      type: 'currency',
      formno: [413],
      question: 'Other Personal Property'
    },
    {
      id: '413-other-assets',
      type: 'currency',
      formno: [413],
      question: 'Other Assets'
    },
    {
      id: 'other-personal-property-block',
      type: 'textarea',
      formno: [413],
      question: 'Describe all the above assets. Describe, and if any is pledged as security, state name and address of lien holder, amount of lien, terms Section 5. Other Personal Property and Other Assets. of payment and if delinquent, describe delinquency. For automobiles, include Year / Make / Model.'
    }
  ],
  [
    {
      id: '413-unpaid-tax',
      type: 'currency',
      formno: [413],
      question: 'Unpaid Taxes'
    },
    {
      id: 'unpaid-taxes-block',
      type: 'textarea',
      formno: [413],
      question: 'Describe in detail, as to type, to whom payable, when due, amount, and to what property, if any, a tax lien attaches.)'
    }
  ],
  [
    {
      id: '413-other-liabilites',
      type: 'currency',
      formno: [413],
      question: 'Other Liabilities'
    },
    {
      id: 'other-liabilites-block',
      type: 'textarea',
      formno: [413],
      question: 'Describe in detail.'
    }
  ],
  [
    {
      id: '413-life-insurance',
      type: 'currency',
      formno: [413],
      question: 'Life Insurance - Cash Surrender Value Only. Describe Below.'
    },
    {
      id: 'life-insurance-held-block',
      type: 'textarea',
      formno: [413],
      question: 'Life Insurance Held - Give face amount and cash surrender value of policies - name of insurance company and beneficiaries'
    }
  ]
];


export default _413D;