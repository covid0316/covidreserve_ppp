import { useState, useEffect } from 'react';
import Link from 'next/link';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { FormStorageHelper } from '../../../lib/storage';
import Layout from '../../../components/layout';
import { Pen, X } from '../../../components/Icons';
import ProgressBar from '../../../components/ProgressBar';
import AddButtonFab from '../../../components/AddButtonFab';

const _4506TFormsSchema = Yup.object().shape({
  name: Yup.string().required("Name is required")
});

type FormName = {
  name: string
};

const FormTitle = '4506-T';

export default () => {
  const [forms, setForms] = useState({});
  const [loading, setLoading] = useState(false);
  const [isAdding, setIsAdding] = useState(false);

  const addForm = () => {
    if (!isAdding) {
      setIsAdding(true);
    }
  };

  const deleteForm = (formName: string) => async (evt: any) => {
    await FormStorageHelper.delete(FormTitle, formName);
    const newForms = {...forms};
    delete newForms[formName];
    setForms(newForms);
  }

  const createForm = async ({ name: formName }: FormName, { resetForm, setErrors }) => {
    if (isAdding) {
      if (Object.keys(forms).indexOf(formName) === -1) {
        await FormStorageHelper.set({}, FormTitle, 0, formName);
        setForms({...forms, [formName]: {}});
        setIsAdding(false);
        resetForm({});
      } else {
        setErrors({ name: 'This form already exists' });
      }
    }
  };

  const isComplete = (value: any): boolean => {
    if (value === null || value === undefined) {
      return false;
    } else if (typeof value === 'string') {
      return value.trim() !== '';
    } else if (typeof value === 'boolean') {
      return value;
    }
    return false;
  };
    
  const percentDone = (form: object): number => {
    if (!form) return 0;
    
    let requiredFields = [
      'primary-name', 'primary-ssn', 'phone-number-52063', 'form-number',
      'current-address', 'p1-month', 'p1-day', 'p1-year',
      'p2-month', 'p2-day', 'p2-year',
      'p3-month', 'p3-day', 'p3-year'
    ];
    if (form['joint-return-yes'] === true) {
      requiredFields = requiredFields.concat([
        'spouse-name', 'spouse-ssn', 'joint-return-yes'
      ]);
    } else {
      requiredFields = requiredFields.concat(['joint-return-no']);
    }

    if (form['previous-address-different-from-current'] === true) {
      requiredFields = requiredFields.concat(['previous-address-different-from-current', 'previous-address']);
    } else {
      requiredFields = requiredFields.concat(['previous-address-same-as-current']);
    }

    if (form['nonfile-yes'] === true) {
      requiredFields = requiredFields.concat(['nonfile-yes']);
    } else {
      requiredFields = requiredFields.concat(['nonfile-no']);
    }

    const fieldsComplete = requiredFields.filter(fieldName => isComplete(form[fieldName]));
    const totalFields = requiredFields.length;
    return fieldsComplete.length / totalFields * 100;
  };

  useEffect(() => {
    setLoading(true);
    FormStorageHelper.get(FormTitle).then(data => {
      setForms(data || {});
      setLoading(false);
    });
  }, []);

  if (loading) {
    return <Layout>Loading...</Layout>
  }

  return (
    <Layout>
      <div className="instructions">
        <p style={{fontWeight: "bold"}}>IRS Form 4506-T needs to be completed and signed by:</p>
        <ul>
          <li>The Applicant business (e.g., your company).</li>
          <li>Each corporation or partnership in which the disaster loan applicant holds a 50% or greater interest</li>
          <li>Each principal owning 20% or more of the applicant business</li>
          <li>Each general partner or managing member</li>
          <li> Any owner who has more than a 50% ownership in an affiliate business. (Affiliates include business parent, subsidiaries, and/or businesses with common ownership or management).
            <p><span style={{fontWeight: "bold"}}>Tip:</span> If your company has changed entity types (example: changed from LLC to C-Corp) in the last 3 years, you will need to fill out multiple Form 4506-Ts for the company, one 4506-T for each tax return type you are requesting.)</p>
          </li>
        </ul>
        <div className="inner">
          <p>Name of Company, Principal, General Partner, Managing Member, or Owner of Affiliate Business</p>
          { Object.entries(forms)
            .sort((a, b) => (a[0] > b[0]) ? 1 : (a[0] == b[0]) ? 0 : -1)
            .filter(e => e[0] !== '_id')
            .map((entry) => {
              const [formName, formContent] = entry;
              return (
                <div className="row">
                  <div><label htmlFor="name">Name</label></div>
                  <div className="item">
                    <Link href={`/form/4506-T/attachment/${encodeURIComponent(formName)}`}>
                      <a>{Pen} {formName}</a>
                    </Link>
                  </div>
                  <ProgressBar amountComplete={percentDone((formContent as object || {})[0])} width={123} size='compact' />
                  <div className="details">
                    <span className="delete" onClick={deleteForm(formName)}>{X}</span>
                  </div>
                </div>
              );
            })
          }
          { isAdding ?
            <Formik initialValues={{
              name: ''
            }} validationSchema={_4506TFormsSchema} onSubmit={createForm}
            >
              {({ errors, touched }) => (
                <Form>
                  <div className="formRow">
                    <div><label htmlFor="name">Name</label></div>
                    <div style={{width: 425}}><Field name="name"/>
                      <div className="validate">
                        {errors.name && touched.name && errors.name}&nbsp;
                      </div>
                    </div>
                    <button type="submit">Create Form</button>
                  </div>
                </Form>
              )}
            </Formik>
            : null
          }
          <AddButtonFab className={isAdding ? 'disabled' : ''} label="Add Another Form 4506-T" onClick={addForm} />
        </div>
      </div>
      <style jsx>
        {`
          .delete {
            cursor: pointer;
          }
          .row {
            display: flex;
            justify-content: flex-start;
          }
          .row .item {
            line-height: 31px;
            padding: 0 20px;
          }
          .row :global(svg) {
            vertical-align: text-bottom;
            display: inline-block;
            position: relative;
            top: .125em;
          }
          .formRow {
            display: flex;
            justify-content: space-between;
          }
          h2 {
            font-size: 48px;
            text-align: center;
            font-weight: normal;
          }
          p {
            font-size: 20px;
            line-height: 142%;
            margin-bottom: 0;
          }
          .center {
            text-align: center;
          }
          li {
            padding-top: 20px;
            font-size: 20px;
          }
          .inner {
            padding-left: 130px;
          }
          .inner p {
            font-size: 16px;
            font-weight: bold;
            margin: 16px 0;
          }
          .instructions :global(.fabbutton.disabled .fab) {
            background: #B1B9C0;
          }
          .instructions :global(.fabbutton.disabled) {
            cursor: default;
          }
          .formRow :global(input) {
            color: #4B6075;
            font-size: 16px;
            font-family: inherit;
            box-sizing: border-box;
            position: relative;
            width: 100%;
            margin: 0;
            border-color: black;
            padding: 5px 20px;
          }
          label {
            font-weight: bold;
            line-height: 31px;
            margin-bottom: 0;
          }
          .validate {
            line-height: 16px;
            color: #D92C23;
            font-size: 12px;
          }
          button {
            padding: 0 40px;
            background: #1382E9;
            font-size: 20px;
            height: 31px;
          }
        `}
      </style>
    </Layout>
  );
};
