/* eslint-disable react/jsx-no-bind */
import React, { useState, useEffect } from 'react';
import { Form } from 'formik';
import { useRouter } from 'next/router';
import formFields from '../../../lib/formFields';
import questions, { TOTAL_PAGES } from '../../../lib/formFields';
import DynamicQuestion from '../../../lib/renderQuestion';
import Layout from '../../../components/layout';
import FormHeader from '../../../components/FormHeader';
import * as storage from '../../../lib/storage';
import Link from 'next/link';
import FormikWrapper from '../../../components/FormikWrapper';
import { Question } from '../../../components/QuestionTypes';
import redirectTo from '../../../lib/redirectTo';
import * as helper from '../../../lib/forms/helper';

function merge(...schemas: Question[]) {
  const [first, ...rest] = schemas;

  const merged = rest.reduce(
    (mergedSchemas, schema) => mergedSchemas.concat(schema.validation!),
    first.validation!
  );

  return merged;
}

const getMergedSchema = (step: number | string) => {
  const schemas = (formFields[step] as Question[]).filter((question: Question) => {
    return !!question.validation;
  });
  if (schemas.length > 0) {
    return merge(...schemas);
  }
}

const getInitialValues = (step: string | number, init: object = {}) =>
  formFields[step].reduce((obj, item: Question) => {
    return helper.getInitialValuesForQuestion(item, obj);
  }, init);

const Basic = ({ ...props }) => {
  const router = useRouter();
  const { step } = router.query;
  const [ loading, setLoading ] = useState(true);
  const [ initialValues, setInitialValues ] = useState({});
  const [ formValues, setFormValues ] = useState({});

  useEffect(() => {
    setLoading(true);
    async function fetchData() {
      if (typeof step === 'string') {
        const formState = await storage.FormStorageHelper.get(5) as object;
        setInitialValues(getInitialValues(step, formState[step]));
        setFormValues({ ...(Object.values(formState).reduce((val, obj) => ({...obj, ...val}), {})), ...initialValues });
        setLoading(false);
      }
    }
    fetchData();
  }, [step]);

  if (loading) {
  return <Layout style={{ padding: '64px 0' }}>{}</Layout>;
  }

  const stepNum = parseInt(step as string);
  const notLast = stepNum + 1 < TOTAL_PAGES;
  const schema = getMergedSchema(stepNum);

  return (
    <Layout style={{ padding: '64px 0' }}>
      <FormHeader step={stepNum + 1} total={TOTAL_PAGES} form={5}/>
      <FormikWrapper
        stepNum={stepNum}
        storageKey={5}
        initialValues={initialValues}
        validationSchema={schema}
      >
        {({ isSubmitting, values, setFieldValue, errors, touched }) => (
          <Form>
            <div>
              {formFields[stepNum].map((question, index) => (
                <DynamicQuestion
                  key={((question as any).id || question.question!) + index}
                  errors={errors}
                  question={question}
                  setFieldValue={setFieldValue}
                  values={values}
                  num={null}
                  className={index > 0 ? 'subquestion' : 'question'}
                />
              ))}
            </div>
            <div>
              <div
                style={{
                  display: 'flex',
                  width: '100%',
                  paddingTop: '48px'
                }}
              >
                {stepNum > 0 && (
                  <div
                    className='previousbutton'
                    onClick={() => {

                      setLoading(true); // Enter loading state before doing async stuff
                      storage.FormStorageHelper.set(values, 5, stepNum);
                    }}
                    style={{ margin: '0 auto 0 64px' }}
                  >
                    <Link href={'[step]'} as={`/form/5/${stepNum-1}`}>
                      <a className='previousbutton'>Previous</a>
                    </Link>
                  </div>
                )}
                  <button
                    type="submit"
                    className='nextbutton'
                    onClick={async () => {
                      // Validate schema
                      try { await schema?.validate(values); } catch (e) { return; }

                      // Schema is valid -- persist form then redirect
                      setLoading(true);
                      if (!notLast) {
                        await storage.FormStorageHelper.set(values, 5, stepNum);
                        // @todo insert completion logic here
                        redirectTo('/form/instructions');
                      } else {
                        await storage.FormStorageHelper.set(values, 5, stepNum);
                        redirectTo(`/form/5/${stepNum + 1}`);
                      }
                    }}
                    style={{ margin: '0 64px 0 auto', cursor: 'pointer' }}
                  >
                    {notLast ? <div>
                      <a className='nextbutton'>{'Next'}</a>
                    </div> : <div>Complete</div>}
                  </button>
              </div>
            </div>
            {/* <button type="button" onClick={async () => {
              console.log(await storage.FormStorageHelper.set({}, 5, stepNum));
            }}>Clear</button> */}
            {/* <pre>{JSON.stringify(values, null, 2)}</pre> */}
            {/* <pre>{JSON.stringify(errors, null, 2)}</pre> */}
            {/* <pre>{JSON.stringify(touched, null, 2)}</pre> */}
          </Form>
        )}
      </FormikWrapper>
    </Layout>
  );
};

export default Basic;
