import React, { useContext, useEffect, useState } from 'react';
import router from 'next/router';
import Link from 'next/link';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import fetchSwal from '../lib/fetchSwal';
import { SignUp, User } from '../components/signup.tsx';
import { UserContext } from '../components/UserContext';
import Layout from '../components/homeLayout';
import redirectTo from '../lib/redirectTo';
import Footer from './Footer';
import Container from '@material-ui/core/Container';

const IndexPage = () => {
  const { dispatch } = useContext(UserContext);
  const [successMessage, setSuccessMessage] = useState('');
  const {
    state: {
      isLoggedIn,
      user: { name },
    },
  } = useContext(UserContext);
  // There may be a better way to redirect when logged in // TODO
  useEffect(() => {
    if (isLoggedIn) {
      router.replace('/form/instructions');
    }
  });

  const handleSubmit = (user) => {
    if (
      (user.lastname.toLowerCase() === 'lim'
      && user.firstname.toLowerCase() === 'jon'
      && user.email.indexOf('jonplim') === 0)
      || user.firstname === 'DEMO'
    ) {
      // Create account
      fetchSwal.post('/api/users', user).then((data) => {
        if (data.ok !== false) {
          dispatch({ type: 'fetch' });
          redirectTo('/form/instructions');
        }
      });
    } else {
      // Send to sales
      fetchSwal.post('/api/sales', user, { noToast: true }).then((data) => {
        if (data.ok !== false) {
          dispatch({ type: 'fetch' });
          setSuccessMessage(
            'Thank you for signing up! We will be in touch shortly.',
          );
        }
      });
    }
  };

  return (
    <Layout>
      <div className="homepage" style={{backgroundColor:'#FFF'}}>
        <div className="hero-img-wrapper">
          <img src="/img/bg2.jpg" className="hero-img" />
          <div className="hero-gradient">
            <Container maxWidth="xl" style={{display: 'flex'}}>
              <div className="hero-tag-wrapper" style={{display: 'inline-block'}}>
              <div className="hero-tag">
                The easiest way for small businesses to apply for COVID-19 SBA
                disaster relief.
              </div>
              <div className="hero-subtag">We specialize in helping small businesses apply for the $50 billion in disaster funding that has been authorized by the USA Federal Government.</div>
              </div>
              <Hidden smDown>
                <div className="float-right">
                  <h2 style={{paddingBottom:'16px', fontWeight: 700}}>Get started for free</h2>
                  <SignUp
                    compact
                    successLabel={successMessage}
                    onSubmit={handleSubmit}
                  />
                </div>
              </Hidden>
              </Container>
            </div>
          </div>
          <Hidden mdUp>
            <div>
              <div
                style={{
                  background: 'white',
                  borderRadius: '5px',
                  padding: '24px',
                }}
              >
                <div className="signupcallout" style={{paddingBottom:'16px', fontWeight: 700}}>Get started for free</div>
                <SignUp
                  compact
                  successLabel={successMessage}
                  onSubmit={handleSubmit}
                />
              </div>
            </div>
          </Hidden>
        <Container maxWidth="xl" style={{paddingBottom: '60px'}}>
        <div className="section1">
          <div className="callout">
            The fastest way to complete your COVID-19 disaster relief
            application.
          </div>
          <div className="table">
            <Grid
              container
              style={{ textAlign: 'center', padding: '0', background: 'white', maxWidth:'1264px'}}
            >
              <Grid item xs={12} sm={12} md={3}>
                <div className="tableitem">
                  <div className="icon">{badgeIcon}</div>
                  <div className="title">Easy-to-Use Tools</div>
                  <div className="tabletext">
                  CovidReserve is the most powerful and flexible tool for applying for an SBA Economic Injury Disaster Loan. Our tools help you complete your application quickly and accurately, so you can submit as soon as possible.
                  </div>
                </div>
              </Grid>
              <Grid item sm={12} md={3}>
                <div className="tableitem">
                  <div className="icon">{circleIcon}</div>
                  <div className="title">Done Right, Guaranteed</div>
                  <div className="tabletext">
                  If the Small Business Administration doesn’t approve your loan application, CovidReserve refunds your fee. Our software conforms with all of the application processes that the SBA requires, including SBA Form 5, SBA Form 1368, SBA Form 413D, and more.
                  </div>
                </div>
              </Grid>
              <Grid item sm={12} md={3}>
                <div className="tableitem">
                  <div className="icon">{lockIcon}</div>
                  <div className="title">Safe and Secure</div>
                  <div className="tabletext">
                  Our software is safe and secure to use. We encrypt all stored data, and when we send your return to the SBA or other agencies, we use SSL encryption that exceeds IRS standards.
                  </div>
                </div>
              </Grid>
              <Grid item sm={12} md={3}>
                <div className="tableitem">
                  <div className="icon">{pencilIcon}</div>
                  <div className="title">How It Works</div>
                  <div className="tabletext">
                  You answer simple questions, and we’ll do all the hard work. Simply tell us about your business, including how your financials were impacted by Covid-19 coronavirus. We’ll guide you through every step.
                  </div>
                </div>
              </Grid>
            </Grid>
          </div>
        </div>
        </Container>
        <div className="commitment" style={{backgroundColor: '#E9E9E9'}}>
          <Grid
            container
            style={{padding: '0', background: '#E9E9E9', maxWidth:'1264px', marginLeft: 'auto', marginRight: 'auto', paddingTop: '40px'}}
          >
            <Hidden smDown>
              <Grid item xs={12} md={6}>
                <img className="testimonyimage" style={{width: '600px'}} src="/img/commitment.png" />
              </Grid>
            </Hidden>
            <Grid item xs={12} md={6} style={{display: 'flex', justifyContent: 'center', alignItems: 'center',}}>
                <div className="commitment-mobile" style={{padding:'24px'}}>
                  <div style={{fontSize:'36px', fontWeight: '700', marginBottom: '8px'}}>Our Commitment</div>
                  <div style={{fontSize:'20px', marginBottom: '32px'}}>We are dedicated to supporting small and medium businesses with radical transparency</div>
                  <Grid style={{display:'flex', marginBottom: '16px'}}>
                    <div style={{display:'inline-block', marginRight: '8px'}}>{inlineCheck}</div>
                    <div style={{display:'inline-block', fontSize:'20px'}}>Free application process for SBA COVID-19 EIDL loans</div>
                  </Grid>
                  <Grid style={{display:'flex', marginBottom: '16px'}}>
                    <div style={{display:'inline-block', marginRight: '8px'}}>{inlineCheck}</div>
                    <div style={{display:'inline-block', fontSize:'20px'}}>Total transparency to our operating costs for any paid products</div>
                  </Grid>
                  <Grid style={{display:'flex', marginBottom: '16px'}}>
                    <div style={{display:'inline-block', marginRight: '8px'}}>{inlineCheck}</div>
                    <div style={{display:'inline-block', fontSize:'20px'}}>Best-in-class data security and the commitment to never share your data</div>
                  </Grid>
                </div>
            </Grid>
          </Grid>
        </div>
        <div className="section2" style={{maxWidth:'1264px'}}>
          <Grid container style={{ padding: '0', background: 'white'}}>
            <Grid
              item
              xs={12}
              sm={12}
              md={6}
              style={{padding: '48px', backgroundColor: '#E9F4FF', display: 'flex', justifyContent: 'center', alignItems: 'center',}}
            >
              <div>
                <div className="quote">{quoteSvg}</div>
                <div className="quoteText">
                  Filling out the SBA standard forms was hard on my own. CovidReserve’s guided application saved me hours of time and gave me peace of mind.
                </div>
                <Link href="">
                  <a className="button" style={{fontSize: '16px'}}>Learn more</a>
                </Link>
              </div>
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <img className="testimonyimage" src="/img/retail.jpg" />
            </Grid>
          </Grid>
        </div>
        <div className="section3">
          <div className="callout">Straightforward pricing, money-back guarantee if your loan application is not approved.*</div>
          <div className="pricing-small">33% off for a limited time.</div>
          <div className="pricing-large">
            <div className="price">$999</div>
            <div className="strike">$1,499</div>
            <div className="unit"> / application</div>
          </div>
          <div className="disclaimer">
            Important offers, pricing details &amp; disclaimers
          </div>
          <Link href="/#">
            <a className="button">Start for free today</a>
          </Link>
        </div>
        <Footer />
        <style jsx>
          {`
        @media only screen and (max-width: 960px) {
          .section3 {
            padding: 40px 0 !important;
          }
          .section3 .pricing-large {
            margin: 10px 0 !important;
          }
          .section3 .price {
            font-size: 42px !important;
          }
          .section3 .strike {
            font-size: 24px !important;
            padding-left: 8px !important;
          }
          .section3 .unit {
            font-size: 18px !important;
            padding-left: 8px !important;
          }
          .section3 .callout {
            font-size: 20px !important;
            padding: 0 40px 20px !important;
          }
          .callout {
            font-size: 24px !important;
          }
          .tabletext {
            margin-left: auto !important;
            margin-right: auto !important;
          }
          .section3 .disclaimer {
            font-size: 16px !important;
            margin-bottom: 25px !important;
          }
          .section3 .pricing-small {
            font-size: 18px !important;
          }
          .section1 {
            padding-bottom: 0 !important;
            margin-top: 20px !important;
            padding-top: 40px !important;
            border-top: 1px solid #DCDDDE !important;
          }
          .commitment-mobile {
            margin-bottom: 40px !important;
          }
          .section2 {
            margin-top: 0 !important;
            margin-bottom: 0 !important;
          }
          .section2 .quoteText {
            font-size: 20px !important;
          }
          .hero-gradient {
            background-color: rgba(0,0,0, 0.30) !important;
          }
          .hero-img-wrapper {
            height: 450px !important;
          }
          section1 .callout {
            padding: 20px !important;
            font-size: 24px !important;
          }
          .hero-tag-wrapper {
            margin: auto 24px 16px !important;
          }
          .hero-tag {
            font-size: 32px !important;
            font-weight: 700 !important;
            line-height: 115%;
          }
          .hero-subtag {
            font-size: 16px !important;
            font-weight: 400 !important;
          }
        }
        .float-right {
          background-color: white;
          box-sizing: border-box;
          border-radius: 5px;
          box-sizing: border-box;
          padding: 40px;
          float: right;
          margin: auto auto auto auto;
          max-width: 450px;
          min-width: 450px;
        }
        .float-right h2 {
          text-align: center;
          font-size: 34px;
          font-weight: normal;
          margin-bottom: 0;
        }
        .float-right h3 {
          text-align: center;
          font-size: 16px;
          font-weight: normal;
          margin-top: 5px;
          margin-bottom: 10px;
          text-align: center;
          color: #4B6075;
        }
        .homepage :global(a.button),
        .homepage :global(a.button:visited),
        .homepage :global(a.button:active) {
          display: inline-block;
          color: white;
          background: #1382E9;
          border-radius: 5px;
          padding: 8px 16px;
          font-size: 20px;
        }
        .signupcallout {
          font-size: 24px;
          text-align: center;
          padding: 0 0 8px;
        }
        .signupsubcallout {
          font-size: 16px;
          text-align: center;
          color: #4B6075;
          padding: 0 0 16px;
        }
        .callout {
          font-color: color: #1F2933;
          font-size: 36px;
          font-weight: bold;
          line-height: 125%;
          text-align: center;
        }
        .hero-tag-wrapper {
          margin: auto 24px 76px auto;
          max-width: 900px;
        }
        .hero-tag {
          color: white;
          font-size: 48px;
          padding: 4px 0;
          width: auto;
          font-weight: 700;
        }
        .hero-subtag {
          padding: 4px 0;
          color: white;
          font-size: 24px;
        }
        .hero-img-wrapper {
          position: relative;
          overflow: hidden;
          width: 100%;
          height: 680px;
        }
        .hero-gradient {
          display: flex;
          position: absolute;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          background: none;
          background-color: rgba(0,0,0, 0.30) !important;
        }
        .hero-img {
          width: 100%;
          height: 100%;
          object-fit: cover;
          object-position: top;
        }
        .section1 {
          background: white;
          padding: 80px 0px 0px 0px;
        }
        .section1 .table {
          margin-top: 32px;
          display: flex;
          flex-direction: row;
          justify-content: space-evenly;
        }
        .section1 .table > div {
          text-align: center;
          width: 300px;
          font-size: 20px;
        }
        .tableitem {
          padding: 24px 24px;
          display: flex;
          flex-direction: column;
        }
        .tabletext {
          max-width: 500px;
          text-align: left;
          font-size: 14px;
        }
        .section1 .table .title {
          font-size: 18px;
          font-weight: bold;
          margin-bottom: 8px;
        }
        .section1 .table .icon {
          display: inline-block;
          padding-bottom: 8px;
        }
        .section2 {
          // background: white;
          display: flex;
          flex-direction: row;
          margin-top: 140px;
          margin: 80px auto 80px auto;
        }
        .section2 .testimonyimage {
          width: 100%;
          height: 100%;
          object-fit: cover;
          max-height: 500px;
        }
        .section2 .left {
          width: 544px;
          font-size: 24px;
          padding: 80px 40px;
        }
        .section2 .quote {
          margin-bottom: 40px;
        }
        .homepage :global(a.button),
        .homepage :global(a.button:visited),
        .homepage :global(a.button:active) {
          display: inline-block;
          color: white;
          background: #1382E9;
          border-radius: 5px;
          padding: 8px 16px;
          font-size: 20px;
        }
        .section2 .quoteText {
          margin-bottom: 50px;
          font-size: 24px;
        }
        .section3 {
          text-align: center;
          padding-bottom: 140px;
        }
        .section3 .pricing-small {
          color: #2CA01C;
          font-size: 34px;
          margin-top: 20px;
        }
        .section3 .pricing-large {
          display: flex;
          margin-top: 20px;
          align-items: center;
          justify-content: center;
        }
        .section3 .price {
          color: #2CA01C;
          font-size: 120px;
          font-weight: bold;
        }
        .section3 .strike {
          font-size: 96px;
          text-decoration: line-through;
          padding-left: 60px;
        }
        .section3 .unit {
          font-size: 48px;
          padding-left: 18px;
        }
        .section3 .disclaimer {
          font-size: 24px;
          margin-bottom: 25px;
        }
      `}
        </style>
        {/* <style jsx>
        {`
          .float-right {
            background-color: white;
            position: absolute;
            right: 50%;
            margin-right: -671px;
            top: 16px;
            border: 1px solid #DCDDDE;
            box-sizing: border-box;
            border-radius: 5px;
            box-sizing: border-box;
            padding: 0 20px;
            min-width: 500px;
            max-width: 650px;
          }
          .float-right h2 {
            font-size: 34px;
            font-weight: normal;
            margin-bottom: 0;
          }
          .float-right h3 {
            font-size: 16px;
            font-weight: normal;
            margin-top: 5px;
            margin-bottom: 10px;
            text-align: center;
            color: #4B6075;
          }
          .inner-column {
            position: relative;
            width: 1200px;
            margin: 0 auto;
          }
          p {
            text-align: center;
            color: #888;
          }
          .hero-tag {
            color: white;
            position: absolute;
            bottom: 52px;
            width: 820px;
            margin-left: -600px;
            left: 50%;
            font-style: normal;
            font-weight: bold;
            font-size: 48px;
            line-height: 142%;
          }
          .hero-img-wrapper {
            position: relative;
            height: 676px;
            overflow: hidden;
            width: 100%;
          }
          .hero-gradient {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            background: none;
            background: linear-gradient(180deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0.2) 51%, rgba(0,0,0,0.45) 100%);
          }
          .hero-img {
            width: 100%;
            min-width: 1824px;
          }
          .section1 {
            background: white;
            padding: 80px 0;
          }
          .callout {
            font-color: color: #1F2933;
            font-size: 48px;
            font-weight: bold;
            line-height: 125%;
            text-align: center;
            padding: 0 80px 20px 80px;
          }
          .section1 .table {
            margin-top: 32px;
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
          }
          .section1 .table > div {
            text-align: center;
            width: 300px;
            font-size: 20px;
          }
          .section1 .table .title {
            font-size: 24px;
            font-weight: bold;
            margin-bottom: 8px;
          }
          .section1 .table .icon {
            display: inline-block;
            height: 80px;
            width: 80px;
            margin-bottom: 24px;
          }
          .section2 {
            // background: white;
            display: flex;
            flex-direction: row;
            margin-top: 140px;
            margin: 140px auto 160px auto;
          }
          .section2 .left {
            width: 544px;
            font-size: 24px;
            padding: 80px 40px;
          }
          .section2 .quote {
            margin-bottom: 40px;
          }
          .homepage :global(a.button),
          .homepage :global(a.button:visited),
          .homepage :global(a.button:active) {
            display: inline-block;
            color: white;
            background: #1382E9;
            border-radius: 5px;
            padding: 12px 16px;
            font-size: 20px;
          }
          .section2 .quoteText {
            margin-bottom: 50px;
          }
          .section3 {
            text-align: center;
            padding-bottom: 140px;
            display: none;
          }
          .section3 .pricing-small {
            color: #2CA01C;
            font-size: 34px;
            margin-top: 20px;
          }
          .section3 .pricing-large {
            display: flex;
            margin-top: 20px;
            align-items: center;
            justify-content: center;
          }
          .section3 .price {
            color: #2CA01C;
            font-size: 120px;
            font-weight: bold;
          }
          .section3 .strike {
            font-size: 96px;
            text-decoration: line-through;
            padding-left: 60px;
          }
          .section3 .unit {
            font-size: 48px;
            padding-left: 18px;
          }
          .section3 .disclaimer {
            font-size: 24px;
            margin-bottom: 25px;
          }
          .section4 {
            background: #116DC1;
            padding: 60px 100px;
            color: white;
            text-align: center;
          }
          .section4 .callout {
            padding: 0;
            padding-bottom: 40px;
          }
          .section4 :global(a.button),
          .section4 :global(a.button:visited),
          .section4 :global(a.button:active) {
            background: white;
            color: #116DC1;
          }
          .footer {
            background: #1F2933;
          }
          .footer .inner {
            display: flex;
            width: 1200px;
            margin: 0 auto;
          }
          .footer :global(a),
          .footer :global(a:visited),
          .footer :global(a:active) {
            color: white;
          }
          ul {
            list-style-type: none;
          }
          li :global(a) {
            font-size: 24px
          }
          li {
            padding: 12px;
          }
        `}
      </style> */}
      </div>
    </Layout>
  );
};

const quoteSvg = (
  <svg
    width="62"
    height="42"
    viewBox="0 0 62 42"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M22.0459 2.87256C22.0459 4.14209 20.8252 6.48584 18.3838 9.90381C15.9912 13.2729 14.7949 15.0552 14.7949 15.2505C14.7949 15.7388 15.2344 16.1782 16.1133 16.5688C16.9922 16.9595 18.0664 17.3989 19.3359 17.8872C20.6055 18.3755 21.875 19.0103 23.1445 19.7915C24.4629 20.5239 25.5615 21.6714 26.4404 23.2339C27.3193 24.7964 27.7588 26.6519 27.7588 28.8003C27.7588 32.5112 26.4648 35.5386 23.877 37.8823C21.3379 40.2261 18.042 41.3979 13.9893 41.3979C10.083 41.3979 6.7627 40.1284 4.02832 37.5894C1.34277 35.0015 0 31.8521 0 28.1411C0 23.8931 0.854492 19.645 2.56348 15.397C4.32129 11.1001 6.64062 7.56006 9.52148 4.77686C12.4512 1.99365 15.4541 0.602051 18.5303 0.602051C20.874 0.602051 22.0459 1.35889 22.0459 2.87256ZM55.8105 2.87256C55.8105 4.14209 54.5898 6.48584 52.1484 9.90381C49.7559 13.2729 48.5596 15.0552 48.5596 15.2505C48.5596 15.6899 48.877 16.0806 49.5117 16.4224C50.1465 16.7642 50.9521 17.0815 51.9287 17.3745C52.9053 17.6675 53.9307 18.1069 55.0049 18.6929C56.1279 19.2788 57.1777 19.9624 58.1543 20.7437C59.1309 21.4761 59.9365 22.5503 60.5713 23.9663C61.2061 25.3823 61.5234 26.9937 61.5234 28.8003C61.5234 32.5112 60.2295 35.5386 57.6416 37.8823C55.1025 40.2261 51.8066 41.3979 47.7539 41.3979C43.8477 41.3979 40.5273 40.1284 37.793 37.5894C35.1074 35.0015 33.7646 31.8521 33.7646 28.1411C33.7646 23.8931 34.6191 19.645 36.3281 15.397C38.0859 11.1001 40.4053 7.56006 43.2861 4.77686C46.2158 1.99365 49.2188 0.602051 52.2949 0.602051C54.6387 0.602051 55.8105 1.35889 55.8105 2.87256Z"
      fill="#1382E9"
    />
  </svg>
);

const circleIcon = (
  <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="32" cy="32" r="32" fill="#1382E9"/>
<path d="M49.4998 30.6283V32.3333C49.4951 40.4525 44.1506 47.6017 36.3646 49.9038C28.5785 52.2059 20.2057 49.1125 15.7865 42.3012C11.3674 35.49 11.9547 26.5833 17.2299 20.4113C22.5052 14.2392 31.2118 12.2721 38.6281 15.5766" stroke="white" stroke-width="3" stroke-linejoin="round"/>
<path d="M30.167 37L29.1063 38.0607L30.167 39.1214L31.2277 38.0607L30.167 37ZM25.7277 30.4394L24.667 29.3787L22.5457 31.5L23.6063 32.5607L25.7277 30.4394ZM49.273 15.7727L29.1063 35.9394L31.2277 38.0607L51.3943 17.894L49.273 15.7727ZM31.2277 35.9394L25.7277 30.4394L23.6063 32.5607L29.1063 38.0607L31.2277 35.9394Z" fill="white"/>
</svg>

);

const clockIcon = (
  <svg
    width="60"
    height="60"
    viewBox="0 0 60 60"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M30 55C43.8071 55 55 43.8071 55 30C55 16.1929 43.8071 5 30 5C16.1929 5 5 16.1929 5 30C5 43.8071 16.1929 55 30 55Z"
      stroke="#1382E9"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M30 15V30L37.5 37.5"
      stroke="#1382E9"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

const textIcon = (
  <svg
    width="60"
    height="60"
    viewBox="0 0 60 60"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M35 5H15C12.2386 5 10 7.23858 10 10V50C10 52.7614 12.2386 55 15 55H45C47.7614 55 50 52.7614 50 50V20L35 5Z"
      stroke="#1382E9"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M35 5V20H50"
      stroke="#1382E9"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M40 32.5H20"
      stroke="#1382E9"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M40 42.5H20"
      stroke="#1382E9"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M25 22.5H22.5H20"
      stroke="#1382E9"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

const lockIcon = (<svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="32" cy="32" r="32" fill="#1382E9"/>
<path d="M23.6787 27.7857V20.7698C23.6787 15.9264 27.6282 12 32.5001 12C37.3721 12 41.3216 15.9264 41.3216 20.7698V27.7857" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M19.5 27.7858V26.2858H18V27.7858H19.5ZM45.5 27.7858H47V26.2858H45.5V27.7858ZM45.5 48.2143V49.7143H47V48.2143H45.5ZM19.5 48.2143H18V49.7143H19.5V48.2143ZM19.5 29.2858H45.5V26.2858H19.5V29.2858ZM44 27.7858V48.2143H47V27.7858H44ZM45.5 46.7143H19.5V49.7143H45.5V46.7143ZM21 48.2143V27.7858H18V48.2143H21Z" fill="white"/>
</svg>


);

const pencilIcon = (<svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="32" cy="32" r="32" fill="#1382E9"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M41.8602 13L51 22.1398L46.7043 26.4355L27.2366 45.9032L17 47L18.0968 36.7634L37.5645 17.2957L41.8602 13Z" fill="#1382E9"/>
<path d="M37.5645 17.2957L18.0968 36.7634L17 47L27.2366 45.9032L46.7043 26.4355M37.5645 17.2957L41.8602 13L51 22.1398L46.7043 26.4355M37.5645 17.2957L46.7043 26.4355" stroke="white" stroke-width="3" stroke-linecap="round"/>
</svg>


);

const badgeIcon = (<svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="32" cy="32" r="32" fill="#1382E9"/>
<path d="M26.2087 35.0599C26.3188 34.2388 25.7424 33.484 24.9213 33.3739C24.1003 33.2638 23.3454 33.8402 23.2353 34.6612L26.2087 35.0599ZM22.5581 51L21.0714 50.8007L20.6594 53.8735L23.3245 52.2894L22.5581 51ZM31.5 45.6852L32.2664 44.3957C31.794 44.115 31.206 44.115 30.7336 44.3957L31.5 45.6852ZM40.4419 51L39.6755 52.2894L42.3402 53.8732L41.9287 50.8009L40.4419 51ZM39.7647 34.6437C39.6548 33.8226 38.9 33.2462 38.0789 33.3561C37.2578 33.4661 36.6813 34.2209 36.7913 35.042L39.7647 34.6437ZM23.64 42.9303L22.1533 42.731L22.1533 42.731L23.64 42.9303ZM31.5 38.2929C39.2202 38.2929 45.5 32.083 45.5 24.3964H42.5C42.5 30.4026 37.5869 35.2929 31.5 35.2929V38.2929ZM45.5 24.3964C45.5 16.7099 39.2202 10.5 31.5 10.5V13.5C37.5869 13.5 42.5 18.3902 42.5 24.3964H45.5ZM31.5 10.5C23.7798 10.5 17.5 16.7099 17.5 24.3964H20.5C20.5 18.3902 25.4131 13.5 31.5 13.5V10.5ZM17.5 24.3964C17.5 32.083 23.7798 38.2929 31.5 38.2929V35.2929C25.4131 35.2929 20.5 30.4026 20.5 24.3964H17.5ZM23.3245 52.2894L32.2664 46.9746L30.7336 44.3957L21.7917 49.7106L23.3245 52.2894ZM30.7336 46.9746L39.6755 52.2894L41.2083 49.7106L32.2664 44.3957L30.7336 46.9746ZM41.9287 50.8009L39.7647 34.6437L36.7913 35.042L38.9552 51.1991L41.9287 50.8009ZM23.2353 34.6612L22.1533 42.731L25.1267 43.1296L26.2087 35.0599L23.2353 34.6612ZM22.1533 42.731L21.0714 50.8007L24.0447 51.1993L25.1267 43.1296L22.1533 42.731Z" fill="white"/>
</svg>
);

const inlineCheck=(<svg width="31" height="25" viewBox="0 0 31 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M30.5607 3.06066C31.1464 2.47487 31.1464 1.52513 30.5607 0.93934C29.9749 0.353553 29.0251 0.353553 28.4393 0.93934L30.5607 3.06066ZM9.5 22L8.43934 23.0607L9.5 24.1213L10.5607 23.0607L9.5 22ZM3.06066 13.4393C2.47487 12.8536 1.52513 12.8536 0.93934 13.4393C0.353553 14.0251 0.353553 14.9749 0.93934 15.5607L3.06066 13.4393ZM28.4393 0.93934L8.43934 20.9393L10.5607 23.0607L30.5607 3.06066L28.4393 0.93934ZM10.5607 20.9393L3.06066 13.4393L0.93934 15.5607L8.43934 23.0607L10.5607 20.9393Z" fill="#1382E9"/>
</svg>
);
export default IndexPage;
